#include "StdAfx.h"
#include "SoupBinTCP.h"
#include "UMDFITCH2FIXConv.h"

#define	PARENT ((Plugin::UMDFITCH2FIXConverter *)m_ParentPtr)

namespace UMDFITCH 
{
	SoupBinTCP::SoupBinTCP(Config lConfig, void * parent)
	{
		m_SoupBinMsgState = 0;
		m_messageSize = 0;
		m_receiveIndex = 0;
		m_ParentPtr = parent;

		m_loopInterval = 0;
		mstr_msgSeqNum = "1";
		mstr_sessionID.clear();

		m_Config = lConfig;
		InitializeStateMachine();
		ConfigureStateMachine();

		_evtRun = CreateEvent(NULL,false,false,NULL);		
		//m_socketControlPtr = (shared_ptr<SocketControl>) new SocketControl(this,lConfig);
		m_socketControlPtr.reset( new SocketControl(this,lConfig) );

		_soupBinTCPThreadPtr.reset ( new boost::thread ( boost::bind (&SoupBinTCP::MainThreadLoop, this)));
	}

	SoupBinTCP::~SoupBinTCP(void)
	{
		::SetEvent ( _evtRun );
		CloseHandle(_evtRun);
	}

	void SoupBinTCP::Start()
	{
		SetOutEvent("EVT_START");
	}

	void SoupBinTCP::Stop()
	{

	}

	void SoupBinTCP::RequestMessage(long sequenceNumber)
	{

	}

	void SoupBinTCP::ParseMessage()
	{

	}

	void SoupBinTCP::InitializeStateMachine()
    {	
		for (int i = 0; i < (int)SIZEOF_STATES; i++)
        {
            for (int j = 0; j < (int) SIZEOF_EVENTS; j++)
            {
				_stateMachineSoupBinTCP[i][j] = st_STATE_MACHINE(SOUPBINTCP::REMAIN, &SoupBinTCP::nop );
            }
        }
    }

	void SoupBinTCP::ConfigureStateMachine()
    {
		/*                    CURRENT STATE                   EVENT 	                            NEXT STATE                           ACTION      */

		_stateMachineSoupBinTCP[ WAIT_TCP_CONNECTION ][     EVT_CONNECTED     ] = st_STATE_MACHINE(   WAIT_LOGIN       ,    &SoupBinTCP::SendLoginRequestInitial   );

		_stateMachineSoupBinTCP[     WAIT_LOGIN      ][   EVT_LOGIN_ACCEPTED  ] = st_STATE_MACHINE(  CONNECTED         ,    &SoupBinTCP::ReadyToReceive            );
		_stateMachineSoupBinTCP[     WAIT_LOGIN      ][   EVT_LOGIN_REJECTED  ] = st_STATE_MACHINE(  WAIT_TO_RECONNECT ,    &SoupBinTCP::WaitToReconnect           );

		_stateMachineSoupBinTCP[   WAIT_TO_RECONNECT ][    EVT_TOUT_LOGIN     ] = st_STATE_MACHINE(   WAIT_LOGIN       ,    &SoupBinTCP::SendLoginRequest          );

		_stateMachineSoupBinTCP[     CONNECTED       ][    EVT_TOUT_HEARTBEAT ] = st_STATE_MACHINE(     CONNECTED      ,       &SoupBinTCP::SendHeartbeat          );
		_stateMachineSoupBinTCP[     CONNECTED       ][ EVT_SOCKET_DATA_PACKET] = st_STATE_MACHINE(     CONNECTED      ,      &SoupBinTCP::ReceiveMessage          );
		_stateMachineSoupBinTCP[     CONNECTED       ][   EVT_END_OF_SESSION  ] = st_STATE_MACHINE( WAIT_TCP_CONNECTION,       &SoupBinTCP::StopAndWait            );
		_stateMachineSoupBinTCP[     CONNECTED       ][    EVT_SOCKET_ERROR   ] = st_STATE_MACHINE( WAIT_TCP_CONNECTION,       &SoupBinTCP::nop					   );

		m_currentState = (int)WAIT_TCP_CONNECTION;
		m_event = (int)SOUPBINTCP::NO_EVENT;
	}

	void SoupBinTCP::MainThreadLoop()
	{
		int actionEvent = SOCKETCONTROL::NO_EVENT;		
		
		while ( WaitForSingleObject( _evtRun, m_loopInterval) != WAIT_OBJECT_0)
		{
			m_state = m_currentState;

			if (m_event != SOUPBINTCP::NO_EVENT)
			{
				if (_stateMachineSoupBinTCP[m_currentState][m_event].i_Proximo_Estado != SOUPBINTCP::REMAIN)
				{
					m_currentState = _stateMachineSoupBinTCP[m_currentState][ m_event].i_Proximo_Estado;
				}
				actionEvent = m_event;

				m_event = SOUPBINTCP::NO_EVENT;

				(this->*_stateMachineSoupBinTCP[m_state][actionEvent].acao)();
				
			}

			MonitorEvents();
		}		
		
	}
	

	void SoupBinTCP::MonitorEvents()
	{
		m_loopInterval = SOUPBINTCP::THREAD_LOOP_INTERVAL;

		if ( m_IncomingMessageQueue.size() > 0 )
		{
			string strEvent = GetInEvent();
			if ( !strEvent.compare("EVT_CONNECTED"))
			{
				m_event = EVT_CONNECTED;
			} 
			else if ( !strEvent.compare("EVT_SOCKET_ERROR"))
			{
				m_event = EVT_SOCKET_ERROR;
			}
			else if ( !strEvent.compare("EVT_SOCKET_DATA"))
			{
				m_loopInterval = 0;
				ReceiveMessage();				
			}
		}


	}	

	/// <summary>
	///	PUSH FRONT
	/// </summary>
	void SoupBinTCP::SetOutEvent(string strEvent)
	{	
		boost::mutex::scoped_lock lockKeeper(m_socketControlPtr->m_IncommingQueueLock);

		m_socketControlPtr->m_IncomingMessageQueue.push_front(strEvent);
	}

	/// <summary>
	///	GET BACK
	/// </summary>
	string SoupBinTCP::GetInEvent()
	{
		string strEvent;

		boost::mutex::scoped_lock lockKeeper(m_IncommingQueueLock);

		strEvent = m_IncomingMessageQueue.back();
		m_IncomingMessageQueue.pop_back();

		return strEvent;
	}


	void SoupBinTCP::nop()
	{

	}

	/// <summary>
	/// Send LOGIN REQUEST with SessionID blank and Requested Sequence Number equal 1.	
	/// </summary>
	void SoupBinTCP::SendLoginRequestInitial()
	{
		m_SoupBinMsgState = 0;
		m_messageSize = 0;
		m_receiveIndex = 0;
		
		strncpy(m_LoginRequestMsg.alfa_UserName, m_Config.m_strPassword.c_str(), sizeof(m_LoginRequestMsg.alfa_UserName));
		strncpy(m_LoginRequestMsg.alfa_Password, m_Config.m_strPassword.c_str(), sizeof(m_LoginRequestMsg.alfa_Password));
		strncpy(m_LoginRequestMsg.alfa_RequestedSession, mstr_sessionID.c_str(), sizeof(m_LoginRequestMsg.alfa_RequestedSession));
		strncpy(m_LoginRequestMsg.num_RequestedSeqNum  , mstr_msgSeqNum.c_str(), sizeof(m_LoginRequestMsg.num_RequestedSeqNum));
		
		WORD packetLength = sizeof(MSG_LOGIN_REQUEST) - sizeof(m_LoginRequestMsg.int_PacketLength);
		m_LoginRequestMsg.int_PacketLength[0] = (char)( (packetLength && 0xff00) >> 8);
		m_LoginRequestMsg.int_PacketLength[1] = (char) (packetLength && 0x00ff) ;
		
		m_socketControlPtr->Send( (unsigned char *)(&m_LoginRequestMsg), sizeof(MSG_LOGIN_REQUEST) );
		
	}

	/// <summary>
	/// Send LOGIN REQUEST with current SessionID Requested Sequence with the last received sequence number.	
	/// </summary>
	void SoupBinTCP::SendLoginRequest()
	{

	}

	/// <summary>
	/// Saves the received SessionID and start a timer for Heartbeat.
	/// </summary>
	void SoupBinTCP::ReadyToReceive()
	{
		mstr_sessionID.assign(m_LoginAccepted.alfa_Session,sizeof(m_LoginAccepted.alfa_Session));		
		mstr_msgSeqNum.assign(m_LoginAccepted.num_SeqNum , sizeof(m_LoginAccepted.num_SeqNum));		
	}

	void SoupBinTCP::ReceiveMessage()
	{
		unsigned long iAvailableBytes = m_socketControlPtr->GetSockAvailableBytes();

		if ( iAvailableBytes > 0 )
		{
			char * p_buffeRecv = (char *)malloc(iAvailableBytes);

			if ( m_socketControlPtr->Receive((unsigned char *)p_buffeRecv,iAvailableBytes) == iAvailableBytes )
			{
				for ( int index = 0 ; index < iAvailableBytes ; index++ )
				{
					if (m_SoupBinMsgState == 0)
					{                    
						m_messageSize = (WORD)((int)p_buffeRecv[index] & 0xff00);                    
						m_receiveBuffer[m_receiveIndex] = p_buffeRecv[index];
						m_receiveIndex++;
						m_SoupBinMsgState = 1;
					} 
					else if ( m_SoupBinMsgState == 1 )
					{
						m_messageSize += (WORD)((int)p_buffeRecv[index] & 0x00ff);
						m_receiveBuffer[m_receiveIndex] = p_buffeRecv[index];
						m_receiveIndex++;
						if (m_messageSize == 0)
						{
							m_receiveIndex = 0;
							continue;
						}
						
						m_SoupBinMsgState = 2;                     
					}
					else if (m_SoupBinMsgState == 2)
					{
						m_receiveBuffer[m_receiveIndex] = p_buffeRecv[index];
						m_receiveIndex++;
						if (m_receiveIndex == (m_messageSize + 2) )
						{
							ParseSoupBinTCPMessage();
							m_SoupBinMsgState = 0;
							m_receiveIndex = 0;
						}
					}		
				}
			}

			free(p_buffeRecv);
		}
		
	}

	/// <summary>
	/// Parses the received data and sends ITCH message buffer to UMDFITCH2FIXConv .
	/// </summary>
	void SoupBinTCP::ParseSoupBinTCPMessage()
	{
		char chMessageType = m_receiveBuffer[2];

		if ( chMessageType == SOUPBINTCP::LOGIN_ACCEPTED )
		{
			memcpy((char *)&m_LoginAccepted, m_receiveBuffer,sizeof(MSG_LOGIN_ACCEPTED));
			m_event = EVT_LOGIN_ACCEPTED;
		} else if ( chMessageType == SOUPBINTCP::SEQUENCED_DATA_PACKET )
		{
			int msgSize = 0;
			msgSize =	m_receiveBuffer[0] << 8;
			msgSize +=	m_receiveBuffer[1];
			
			m_SequencedDataPacket.p_Message = (char *)malloc(msgSize - 1);
			memset(m_SequencedDataPacket.p_Message,' ',sizeof(m_SequencedDataPacket.p_Message));

			memcpy((char *)&m_SequencedDataPacket, m_receiveBuffer, 3);			
			memcpy((char *)m_SequencedDataPacket.p_Message, (char *)&m_receiveBuffer[3],msgSize - 1);
			
			PARENT->ExecuteConvertion( (unsigned char *)m_SequencedDataPacket.p_Message, msgSize - 1);
			free(m_SequencedDataPacket.p_Message);
			
		}
	}
	void SoupBinTCP::WaitToReconnect()
	{

	}


	void SoupBinTCP::SendHeartbeat()
	{

	}

	/// <summary>
	/// Sends EVT_STOP to Event Bus
	/// </summary>
	void SoupBinTCP::StopAndWait()
	{

	}
};
