#pragma once
#include "Config.h"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/random_access_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include "Util.h"
#include "StrX.h"
#include "ITCHCracker.h"
#include "FIXAssembler.h"

namespace UMDFITCH 
{
	using std::string;
	using std::map;
	using std::wstring;
	using boost::multi_index_container;
	using namespace boost::multi_index;
	XERCES_CPP_NAMESPACE_USE;	

	class Convertions
	{
	private:
		Config m_Config;

		XERCES_CPP_NAMESPACE::DOMDocument * m_docITCH2FIX;

		void LoadITCH2FIXConvertions();
		void LoadMappings();
		
		ListMappings		m_listMappings;
		map<string,string>	m_dictVariables;
	public:
		Convertions(void);
		Convertions(Config lConfig);
		~Convertions(void);				

		void ExecuteDirectConvertion(shared_ptr<ITCHCracker> crackerPtr, shared_ptr<FIXAssembler> assemblerPtr);
	};
};