#pragma once

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "ITCHDictionary.h"
#include "Config.h"
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include "MessageComponent.h"
#include "Util.h"

namespace UMDFITCH 
{
	using std::string;
	using boost::shared_ptr;
	using boost::multi_index_container;
	using namespace boost::multi_index;		


	class ITCHCracker
	{
			
	public:
		string					m_MessageName;
		string					m_MessageType;
		
		typedef multi_index_container<		
			MessageComponent,
			indexed_by<			
				hashed_unique< member< MessageComponent,std::string,&MessageComponent::m_Name> >
			>
		> DictNameToMessageComponent;
		DictNameToMessageComponent m_dictMessageComponent;
		//DictKeyFields	m_dictKeyFields;

		ITCHCracker(void);
		ITCHCracker(Config lConfig);
		~ITCHCracker(void);
		
		void Crack(unsigned char * buffer , int size);
		int	 m_iCurrentPosition;

		/// <summary>
		/// ITCH Messages from ITCHMessages.xml
		/// </summary>
		shared_ptr<ITCHDictionary>	ITCHDictionaryPtr;
		Config						m_Config;

		

	};

};