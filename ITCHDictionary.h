#pragma once
#include "Config.h"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include "Util.h"
#include "StrX.h"

namespace UMDFITCH 
{
	using std::string;
	using std::wstring;
	using boost::multi_index_container;
	using namespace boost::multi_index;
	XERCES_CPP_NAMESPACE_USE;	

	class ITCHDictionary
	{
	private:
		Config m_Config;

		XERCES_CPP_NAMESPACE::DOMDocument * m_document;
		
		void LoadITCHMessages();
		
		void LoadFields ();
		void LoadMessages();
	public:
		ITCHDictionary(void);
		ITCHDictionary(Config lConfig);
		~ITCHDictionary(void);

		/// <summary>
		/// ITCHMessages.xml
		/// </summary>
		typedef multi_index_container<
			shared_ptr<MESSAGE>,
			indexed_by<
			hashed_unique<member<MESSAGE,std::string,&MESSAGE::Type>>
			>
		> Messages_set;

		typedef multi_index_container<
			FIELD,
			indexed_by<
			hashed_unique<member<FIELD, std::string, &FIELD::Name> >
			>
		> Fields_set;						

		Messages_set	m_Messages;
		Fields_set		m_Fields;		

	};

};