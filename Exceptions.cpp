#include "stdafx.h"
#include <tchar.h>
#include <xercesc/dom/DOMError.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>
#include <xercesc/dom/DOMLocator.hpp>
#include <xercesc/sax/SAXParseException.hpp>
#include <strstream>
#include "StrX.h"
#include "Exceptions.h"

XERCES_CPP_NAMESPACE_USE;

namespace UMDFITCH 
{
	using std::stringstream;
	
	CXMLDebugErrorHandler::CXMLDebugErrorHandler() : XERCES_CPP_NAMESPACE::ErrorHandler(), sawErrors (false)
	{
	}
	
	CXMLDebugErrorHandler::~CXMLDebugErrorHandler()
	{
	}
	
	void CXMLDebugErrorHandler::warning(const SAXParseException& exc)
	{
		handleError (exc, DOMError::DOM_SEVERITY_WARNING);
	}
	
	void CXMLDebugErrorHandler::error(const SAXParseException& exc)
	{
		handleError (exc, DOMError::DOM_SEVERITY_ERROR);
	}
	
	void CXMLDebugErrorHandler::fatalError(const SAXParseException& exc)
	{
		handleError (exc, DOMError::DOM_SEVERITY_FATAL_ERROR);
	}
	
	void CXMLDebugErrorHandler::resetErrors()
	{
		sawErrors = false;
		details.clear();
	}
	
	void CXMLDebugErrorHandler::handleError(const SAXParseException& saxError, short severity)
	{
		stringstream ss;
		if (!sawErrors)
			details.assign ("XML Parsing Error(s): ");

		ss << _T("{");
		if (severity == XERCES_CPP_NAMESPACE::DOMError::DOM_SEVERITY_WARNING)
			ss << _T("Warning at file ");
		else if (severity == XERCES_CPP_NAMESPACE::DOMError::DOM_SEVERITY_ERROR)
			ss << _T("Error at file ");
		else
			ss << _T("Fatal Error at file ");

		ss << StrX::toString (saxError.getSystemId())
		   << _T(", line ") << saxError.getLineNumber()
		   << _T(", char ") << saxError.getColumnNumber()
		   << _T("  Message: ") << StrX(saxError.getMessage()) << _T("}");

		if (sawErrors) 
			details.append(_T(", "));
		details.append(ss.str());
		sawErrors = true;
	}
};