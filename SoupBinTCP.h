#pragma once
#include "stdafx.h"
#include "INASDAQReceiver.h"

#pragma warning (disable : 4146 4267 4996)
#include <boost/thread.hpp>

#include "SocketControl.h"
#include "Config.h"

namespace UMDFITCH 
{
	using boost::shared_ptr;

	namespace SOUPBINTCP
	{
		const int REMAIN = -1;
		const int NO_EVENT = -1;
		const int THREAD_LOOP_INTERVAL = 8;
		const int MAX_BUFFER_SIZE = 20000;

		const char LOGIN_ACCEPTED = 'A';
		const char LOGIN_REQUEST = 'L';
		const char SEQUENCED_DATA_PACKET = 'S';
	};

	class SoupBinTCP : public INASDAQReceiver
	{
	public:		

		SoupBinTCP(Config lConfig, void * parent);		
		~SoupBinTCP(void);

		void Start();
		void Stop();
		void RequestMessage(long sequenceNumber);
		void ParseMessage();

		int					m_SoupBinMsgState;
		int					m_state;
		int					m_currentState;
		int					m_event;
		HANDLE				_evtRun;
		Config				m_Config;
		boost::mutex		m_IncommingQueueLock;
		std::deque<string>	m_IncomingMessageQueue;

		enum STATES
		{
			WAIT_TCP_CONNECTION=0,
			WAIT_LOGIN,
			WAIT_TO_RECONNECT,
			CONNECTED,
			SIZEOF_STATES
		};

		enum EVENTS
		{			
			EVT_CONNECTED = 0,
			EVT_TOUT_LOGIN,
			EVT_LOGIN_ACCEPTED,
			EVT_LOGIN_REJECTED,
			EVT_TOUT_HEARTBEAT,
			EVT_SOCKET_DATA_PACKET,
			EVT_SOCKET_ERROR,
			EVT_END_OF_SESSION,
			SIZEOF_EVENTS
		};

		typedef void (SoupBinTCP::* MPFUNC)(void);	

		typedef struct STATE_MACHINE 
		{
			int i_Proximo_Estado;
			MPFUNC acao;
			
			STATE_MACHINE(){}
			STATE_MACHINE(char proximo_Estado, MPFUNC lAcao)
			{
				acao = lAcao;
				i_Proximo_Estado = proximo_Estado;
			}
			
		} st_STATE_MACHINE;

		st_STATE_MACHINE _stateMachineSoupBinTCP[SIZEOF_STATES][SIZEOF_EVENTS];

		boost::shared_ptr<boost::thread> _soupBinTCPThreadPtr;

		boost::shared_ptr<SocketControl> m_socketControlPtr;
		
		
		void	SetOutEvent(string strEvent);
		string	GetInEvent();

		//
		// SoupBinTCP messages structs
		//

		typedef struct LOGIN_REQUEST
		{
			LOGIN_REQUEST()
			{
				ch_PacketType = SOUPBINTCP::LOGIN_REQUEST;
				memset(alfa_UserName,' ',sizeof(alfa_UserName));
				memset(alfa_Password,' ',sizeof(alfa_Password));
				memset(alfa_RequestedSession,' ',sizeof(alfa_RequestedSession));
				memset(num_RequestedSeqNum,0x00,sizeof(num_RequestedSeqNum));
			}


			char int_PacketLength[2];
			char ch_PacketType;
			char alfa_UserName[6];
			char alfa_Password[10];
			char alfa_RequestedSession[10];
			char num_RequestedSeqNum[20];			

		} MSG_LOGIN_REQUEST;
		MSG_LOGIN_REQUEST	m_LoginRequestMsg;

		typedef struct LOGIN_ACCEPTED
		{
			LOGIN_ACCEPTED()
			{
				memset(alfa_Session,0x00,sizeof(alfa_Session));
				memset(num_SeqNum,0x00,sizeof(num_SeqNum));
			}
			char int_PacketLength[2];
			char ch_PacketType;
			char alfa_Session[10];
			char num_SeqNum[20];			
		} MSG_LOGIN_ACCEPTED;
		MSG_LOGIN_ACCEPTED m_LoginAccepted;

		typedef struct SEQUENCED_DATA_PACKET
		{
			char	int_PacketLength[2];
			char	ch_PacketType;
			char *	p_Message;			
		} MSG_SEQUENCED_DATA_PACKET;

		MSG_SEQUENCED_DATA_PACKET m_SequencedDataPacket;

	private:
		

		void InitializeStateMachine();
		void ConfigureStateMachine();
		void MainThreadLoop();
		void MonitorEvents();

		char 		m_receiveBuffer[SOUPBINTCP::MAX_BUFFER_SIZE];
        int			m_receiveIndex;
		int			m_messageSize;
		void		ParseSoupBinTCPMessage();

		//
		// STATE MACHINE FUNCTIONS
		//
		void nop();
		void SendLoginRequest();
		void SendLoginRequestInitial();
		void ReadyToReceive();
		void ReceiveMessage();
		void WaitToReconnect();
		void SendHeartbeat();
		void StopAndWait();

		DWORD		m_loopInterval;
		string		mstr_sessionID;
		string		mstr_msgSeqNum;	
		void *		m_ParentPtr;

	};

};