#pragma once
#include "INASDAQReceiver.h"

namespace UMDFITCH 
{
	class MoldUDP64 : public INASDAQReceiver
	{
	public:
		MoldUDP64(void);
		~MoldUDP64(void);

		void Start();
		void Stop();
		void RequestMessage(long sequenceNumber);
		void ParseMessage();
	};

};