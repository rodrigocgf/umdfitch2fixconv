#include "StdAfx.h"

#include <xercesc/dom/DOMAttr.hpp>
#include <xercesc/dom/DOMConfiguration.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <xercesc/dom/DOMError.hpp>
#include <xercesc/dom/DOMErrorHandler.hpp>
#include <xercesc/dom/DOMImplementation.hpp>
#include <xercesc/dom/DOMImplementationLS.hpp>
#include <xercesc/dom/DOMImplementationRegistry.hpp>
#include <xercesc/dom/DOMLocator.hpp>
#include <xercesc/dom/DOMLSParser.hpp>
#include <xercesc/dom/DOMNamedNodeMap.hpp>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMNodeList.hpp>
#include <xercesc/dom/DOMXPathEvaluator.hpp>
#include <xercesc/dom/DOMXPathException.hpp>
#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/util/Base64.hpp>
#include <xercesc/util/PlatformUtils.hpp>
#include <xercesc/util/XMLString.hpp>
#include <xercesc/util/XMLUni.hpp>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/case_conv.hpp>
#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <cstdarg>
#include "Util.h"
#include "StrX.h"
#include "Exceptions.h"
#include <quickfix/values.h>
//#include "DecNumber.hpp"

XERCES_CPP_NAMESPACE_USE;


namespace UMDFITCH 
{

	FIELD& FIELD::operator=(const FIELD& in)
	{
		this->Name = in.Name;
		this->Value = in.Value;
		this->Position = in.Position;
		this->Length = in.Length;
		this->Number = in.Number;
		this->Type = in.Type;
		this->Description = in.Description;

		return *this;
	}

	string Util::getAttribute (DOMElementPtr element, const wstring& attributeName) 
	{
		return StrX::toString (element->getAttribute (attributeName.c_str()));
	}
	
	string Util::getAttribute (DOMElementPtr element, const wstring& attributeName, const string& defaultValue) 
	{
		DOMAttr* pAttr = element->getAttributeNode (attributeName.c_str());
		return pAttr ? StrX::toString (pAttr->getValue()) : defaultValue;
	}
	
	bool Util::verifyTypeOf(DOMElementPtr parentElement , const string& elementName)
	{
		if ( StrX::equals (parentElement->getNodeName(), elementName) )
			return true;
		else
			return false;		
	}

	bool Util::hasElement(DOMElementPtr parentElement , const wstring& elementName)
	{
		DOMNodeList* pAttr = parentElement->getElementsByTagName(elementName.c_str());
		if ( pAttr == NULL )
			return false;

		if ( pAttr->getLength() > 0 )
			return true;
		else
			return false;		
	}

	const DOMElement * Util::childElement(const DOMElement * root )
	{
		DOMNode * pChild = root->getFirstChild();
		if (pChild->getNodeType() == DOMNode::ELEMENT_NODE)
		{
			DOMElement *childElement = dynamic_cast<DOMElement*> (pChild);
			return childElement;
		}

		return NULL;		
	}

	vector<const DOMElement*> Util::childrenElements (const DOMElement* root)
	{
		vector<const DOMElement*> ret;
		DOMNodeList* children = root->getChildNodes ();
		for (XMLSize_t i = 0, n = children->getLength(); i < n; ++i)
		{
			DOMNode* child = children->item(i);
			if (child->getNodeType() == DOMNode::ELEMENT_NODE)
			{
				DOMElement *childElement = dynamic_cast<DOMElement*> (child);
				if (childElement)
				{
					ret.push_back(childElement);
				}
			}
		}

		return ret;
	}
	
	vector<const DOMElement*> Util::childrenElements (const DOMElement* root, const string& nodeName)
	{
		vector<const DOMElement*> ret;
		DOMNodeList* children = root->getChildNodes ();
		for (XMLSize_t i = 0, n = children->getLength(); i < n; ++i)
		{
			DOMNode* child = children->item(i);
			if (child->getNodeType() == DOMNode::ELEMENT_NODE)
			{
				DOMElement *childElement = dynamic_cast<DOMElement*> (child);
				if (childElement && StrX::equals (childElement->getNodeName(), nodeName))
				{
					ret.push_back(childElement);
				}
			}
		}

		return ret;
	}
	

	vector<const DOMElement*> Util::evalXPath (const DOMDocument* document, const wstring& xpathExpression)
	{
		DOMXPathEvaluator *xpe = const_cast<DOMXPathEvaluator *>(dynamic_cast<const DOMXPathEvaluator *>(document));
	
		vector<const DOMElement*> ret;
		try {			

			DOMXPathResult* result = xpe->evaluate (xpathExpression.c_str(), document->getDocumentElement(),
				NULL, DOMXPathResult::ORDERED_NODE_SNAPSHOT_TYPE, NULL);
			for (int i = 0; result->snapshotItem (i); ++i)
			{
				DOMElement* element = dynamic_cast<DOMElement*> (result->getNodeValue ());
				if (element) {
					ret.push_back (element);
				}
			}

			result->release ();
		} catch (DOMXPathException& ) {
			;
		} catch (DOMException& ) {
			;
		}
		return ret;
	}
	
	bool Util::xerces_initialized = false;
	
	void Util::parseXML(const string& filename, bool validate, XERCES_CPP_NAMESPACE::DOMDocument*& document)
	{
		if (!xerces_initialized)
		{
			try {
				XMLPlatformUtils::Initialize();
			} catch (const XMLException& ex) {
				throw XercesInitializationError () 
				<< descr_info (StrX::toString(ex.getMessage())) 
				<< filelocation_info (str_file_location (ex.getSrcFile(), (int) ex.getSrcLine()))
				<< PROGRAMLOCATION();
			}
			xerces_initialized = true;
		}    
		XercesDOMParser *parser = new XercesDOMParser();
		if (validate)
		{
			parser->setDoNamespaces (true);
			parser->setDoSchema (true);
			parser->setValidationSchemaFullChecking (true);
		}
		// Create error handler
		CXMLDebugErrorHandler errorHandler;
		parser->setErrorHandler (&errorHandler);
		errorHandler.resetErrors();
		// Parse the file
		document = NULL;
		try 
		{
			parser->parse (filename.c_str());
			document = parser->getDocument();
		}
		catch (const XMLException& ex)
		{
			throw InvalidTemplateError() 
				<< descr_info (StrX::toString(ex.getMessage())) 
				<< filelocation_info (str_file_location (ex.getSrcFile(), (int) ex.getSrcLine()))
				<< PROGRAMLOCATION();
		}
		catch (const DOMException& ex)
		{
			throw InvalidTemplateError() 
				<< descr_info (StrX::toString(ex.getMessage())) 
				<< code_info (ex.code)
				<< PROGRAMLOCATION();
		}
		catch (...)
		{
			throw InvalidTemplateError() 
				<< descr_info ("Unexpected error!") 
				<< PROGRAMLOCATION();
		}
		parser->reset();
	}
	
	bool Util::booleanValue (const string& str)
	{
		if (str.empty()) return false;
		if (boost::algorithm::istarts_with (str, "T") 
		 || boost::algorithm::istarts_with (str, "V")
		 || boost::algorithm::istarts_with (str, "Y")) return true;
		if (atoi (str.c_str()) != 0) return true;
		return false;
	}
	
	string Util::format (const char* fmt, ...)
	{
		va_list arglist;
		va_start (arglist, fmt);
		char szBuf [10*1024];
		vsprintf_s (szBuf, sizeof(szBuf), fmt, arglist);
		va_end (arglist);
		return szBuf;
	}
	
	int Util::parseInt (const string& str)
	{
		return str.empty() ? -1 : atoi (str.c_str());
	}
	
	// UTCTimestamp : YYYYMMDD-HH:MM:SS.sss
	string Util::itchTimeToUTCSystemTime ( const string & secondsSinceMidNight, const string & nanoseconds )
	{
		double dSencondsSinceMidnight = 0;
		
		for ( int j = 0, i = secondsSinceMidNight.length() - 1 ; i >= 0  ; i-- , j++)
		{
			dSencondsSinceMidnight += atoi(string(1,secondsSinceMidNight[i]).c_str())*pow((double)10,(int)j);
		}

		double dnanoseconds = 0;
		for ( int j = 0, i = nanoseconds.length() - 1 ; i >= 0  ; i--, j++ )
		{
			dnanoseconds += atoi(string(1,nanoseconds[i]).c_str())*pow((double)10,(int)j);
		}

		int hour = (int)(dSencondsSinceMidnight/3600);
		int min = (int)((dSencondsSinceMidnight - hour*3600)/60);
		int sec = (int)(dSencondsSinceMidnight - hour*3600 - min*60);
		//int msec = (int)(dnanoseconds/1000);

		SYSTEMTIME localTime;
		::GetLocalTime(&localTime);

		return Util::format("%04d%02d%02d-%02d:%02d:%02d.%s",localTime.wYear, localTime.wMonth, localTime.wDay,hour,min,sec,nanoseconds.c_str());
		
	}

	void Util::localStringTimestampToUTCSystemTime (const string& str, SYSTEMTIME& st)
	{
		// Converte um hor�rio YYYYMMDDHHMMSS ou YYYYMMDDHHMMSS999999 (hor�rio local)
		// para o hor�rio UTC.
		SYSTEMTIME localST;
		DWORD microseconds = 0;
		memset (&localST, 0, sizeof (localST));
		sscanf_s (str.c_str(), 
			"%04hd%02hd%02hd%02hd%02hd%02hd%06d", 
			&localST.wYear, &localST.wMonth, &localST.wDay, 
			&localST.wHour, &localST.wMinute, &localST.wSecond, &microseconds);
		localST.wMilliseconds = (WORD) (microseconds / 1000);
		FILETIME localFT, ft;
		::SystemTimeToFileTime (&localST, &localFT);
		::LocalFileTimeToFileTime (&localFT, &ft);
		::FileTimeToSystemTime (&ft, &st);
	}
	
	void Util::localTimeToUTCSystemTime (const string& str, SYSTEMTIME& st)
	{
		// Converte um hor�rio HHMMSS ou HHMMSS999999 (hor�rio local)
		// para o hor�rio UTC.
		SYSTEMTIME localST;
		::GetSystemTime (&localST);
		DWORD microseconds = 0;
		sscanf_s (str.c_str(), 
			"%02hd%02hd%02hd%06d", 
			&localST.wHour, &localST.wMinute, &localST.wSecond, &microseconds);
		localST.wMilliseconds = (WORD) (microseconds / 1000);
		FILETIME localFT, ft;
		::SystemTimeToFileTime (&localST, &localFT);
		::LocalFileTimeToFileTime (&localFT, &ft);
		::FileTimeToSystemTime (&ft, &st);
	}

	string Util::toFIXDate (const string& str)
	{
		// YYYYMMDDHHMMSS -> YYYYMMDD
		SYSTEMTIME st;
		localStringTimestampToUTCSystemTime (str, st);
		return Util::format ("%04d%02d%02d", st.wYear, st.wMonth, st.wDay);
	}
	
	string Util::toFIXTime (const string& str)
	{
		// Aten��o - note que o hor�rio do RLC � local, mas devemos converter para o hor�rio UTC.
		// YYYYMMDDHHMMSS -> HHMMSS
		SYSTEMTIME st;
		localStringTimestampToUTCSystemTime (str, st);
		return Util::format ("%02d%02d%02d", st.wHour, st.wMinute, st.wSecond);
	}
	
	string Util::rLCTimeToFIXTime (const string& str)
	{
		SYSTEMTIME st;
		localTimeToUTCSystemTime (str, st);
		return Util::format ("%02d%02d%02d", st.wHour, st.wMinute, st.wSecond);
	}
	
	string Util::rLCTimeToFIXTimestamp (const string& str)
	{
		SYSTEMTIME st;
		localTimeToUTCSystemTime (str, st);
		return Util::format ("%04d%02d%02d-%02d%02d%02d", 
			st.wYear, st.wMonth, st.wDay, 
			st.wHour, st.wMinute, st.wSecond);
	}
	
	char Util::rLCSignToFIXTickDirection (const string& str)
	{
		char tickDirection = FIX::TickDirection_ZERO_PLUS_TICK;
		switch (str[0]) {
		case '+':
			tickDirection = FIX::TickDirection_PLUS_TICK;
			break;
		case '-':
			tickDirection = FIX::TickDirection_MINUS_TICK;
			break;
		case '0':
			tickDirection = FIX::TickDirection_ZERO_PLUS_TICK;
			break;
		}
		return tickDirection;
	}
	
	void Util::toFIXDateAndTime (const string& str, string& date, string& time)
	{
		SYSTEMTIME st;
		localStringTimestampToUTCSystemTime (str, st);
		date = Util::format ("%04d%02d%02d", st.wYear, st.wMonth, st.wDay);
		time = Util::format ("%02d%02d%02d", st.wHour, st.wMinute, st.wSecond);
	}
	
	void Util::toMDEntryDateAndTime (const string& str, FIX::MDEntryDate& date, FIX::MDEntryTime& time)
	{
		SYSTEMTIME st;
		localStringTimestampToUTCSystemTime (str, st);
		date.setString (Util::format ("%04d%02d%02d", st.wYear, st.wMonth, st.wDay));
		time.setString (Util::format ("%02d%02d%02d", st.wHour, st.wMinute, st.wSecond));
	}
	
	string Util::toFIXTimestamp (const string& str)
	{
		// YYYYMMDDHHMMSS -> YYYYMMDD-HH:MM:SS ou YYYYMMDD-HH:MM:SS.sss
		SYSTEMTIME st;
		localStringTimestampToUTCSystemTime (str, st);
		if (st.wMilliseconds) {
			return Util::format ("%04d%02d%02d-%02d:%02d:%02d.%03d", st.wYear, st.wMonth, st.wDay,
				st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
		} else {
			return Util::format ("%04d%02d%02d-%02d:%02d:%02d", st.wYear, st.wMonth, st.wDay,
				st.wHour, st.wMinute, st.wSecond);
		}
	}
	
	/// <example> 
	/// &lt;time&gt; source="Order Timestamp"	&lt;__expr__&gt; 
	/// </example>
	FIX::UtcTimeStamp Util::toUtcTimestamp (const string& str)
	{
		// YYYYMMDDHHMMSS -> YYYYMMDD-HH:MM:SS ou YYYYMMDD-HH:MM:SS.sss
		SYSTEMTIME st;
		localStringTimestampToUTCSystemTime (str, st);
		return FIX::UtcTimeStamp (st.wHour, st.wMinute, st.wSecond, st.wMilliseconds, st.wDay, st.wMonth, st.wYear);
	}
	
	string Util::lpad (const string& str, size_t length, const string& fill)
	{
		// http://www.adp-gmbh.ch/ora/sql/rpad.html
		// lpad ("1234567890", 7, " ") --> "1234567"
		// lpad('string', 15, '.,_') --> ".,_.,_.,_string"

		const size_t strSize = str.size();
		if (strSize > length) {
			return str.substr (0, length);
		} else if (strSize == length) {
			return str;
		} else {
			if (fill.empty() || fill == " ") {
				return string (length - strSize, ' ') + str;
			} 
			const size_t fillSize = fill.size();
			size_t padLength = length - str.size();
			size_t padRepetitions = (padLength + fillSize - 1) / fillSize;
			string pad;
			pad.reserve (padRepetitions * fillSize);
			for (size_t i = 0; i < padRepetitions; ++i) {
				pad.append (fill);
			}
			return pad.substr (0, padLength) + str;
		}
	}
	
	string Util::rpad (const string& str, size_t length, const string& fill)
	{
		// http://www.adp-gmbh.ch/ora/sql/rpad.html
		// lpad ("1234567890", 7, " ") --> "1234567"
		// lpad('string', 15, '.,_') --> ".,_.,_.,_string"

		const size_t strSize = str.size();
		if (strSize > length) {
			return str.substr (0, length);
		} else if (strSize == length) {
			return str;
		} else {
			if (fill.empty() || fill == " ") {
				return str + string (length - strSize, ' ');
			} 
			const size_t fillSize = fill.size();
			size_t padLength = length - str.size();
			size_t padRepetitions = (padLength + fillSize - 1) / fillSize;
			string pad;
			pad.reserve (padRepetitions * fillSize);
			for (size_t i = 0; i < padRepetitions; ++i) {
				pad.append (fill);
			}
			return str + pad.substr (0, padLength);
		}
	}
	
	string Util::zerofill (const string& str, size_t length)
	{
		char *endPtr = 0;
		__int64 val = _strtoi64 (str.c_str(), &endPtr, 10);
		if (endPtr == str.c_str() + str.size()) {
			// O valor � realmente num�rico, ent�o usar sprintf_s
			char szBuf[64];
			sprintf_s (szBuf, sizeof (szBuf), "%0*I64d", length, val);
			return szBuf;
		} else {
			// Completar com zeros
			return lpad (str, length, " ");
		}
	}
	
	string Util::capitalize (const string& str) 
	{
		if (!str.empty()) {
			string firstChar (str.substr (0, 1));
			boost::algorithm::to_upper (firstChar);
			return firstChar + str.substr (1);
		} else {
			return str;
		}
	}
	
	string Util::normalize (const string& str)
	{
		// "Creation of Instrument Characteristics" -> "CreationOfInstrumentCharacteristics"
		// "Start / End of Market Sheet Broadcasting" -> "StartEndOfMarketSheetBroadcasting"
		// "Last Adjusted &amp; Super-adjusted Closing Price" -> "LastAdjustedAndSuperAdjustedClosingPrice"
		string strCopy (boost::algorithm::replace_all_copy (str, "&", "And"));
		vector<string> words;
		boost::algorithm::split (words, strCopy, ! boost::algorithm::is_alnum (), boost::algorithm::token_compress_on);
		for (size_t i = 0; i < words.size(); ++i) {
			words[i] = capitalize (words[i]);
		}
		return boost::algorithm::join (words, "");
	}
	
	string Util::right (const string& str, size_t n)
	{
		return str.length() <= n ? str : str.substr (str.length() - n);
	}

	
	string Util::toFormattedString (const string& msgType, const FIX::FieldMap& fmap, const FIX::DataDictionary& dd, int level, const FIX::DataDictionary& globalDD) {
		string ret;
		for (FIX::FieldMap::iterator it = fmap.begin(); it != fmap.end(); ++it) {
			int fieldNo = it->first;
			string value = it->second.getString();
			string fieldName; globalDD.getFieldName(fieldNo, fieldName);
			string valueName; globalDD.getValueName(fieldNo, value, valueName);
			if (!fieldName.empty())
				ret.append (level * 2, ' ').append (Util::format ("%d:%s=%s", fieldNo, fieldName.c_str(), value.c_str()));
			else
				ret.append (level * 2, ' ').append (Util::format ("%d=%s", fieldNo, value.c_str()));
			if (!valueName.empty())
				ret.append (Util::format (" [%s]\n", valueName.c_str()));
			else 
				ret.append ("\n");

			FIX::TYPE::Type type;
			if (dd.getFieldType (it->first, type) && type == FIX::TYPE::NumInGroup) {
				int groupField = it->first;
				int groupCount = lexical_cast<int>(it->second.getString());
				int delim = 0;
				const FIX::DataDictionary *pDD = 0;
				dd.getGroup (msgType, groupField, delim, pDD);
				for (int i = 1; i <= groupCount; ++i) {
					FIX::Group group (groupField, delim);
					fmap.getGroup (i, it->first, group);
					ret.append (level * 2, ' ').append (Util::format ("[%d]\n", i));
					ret.append (toFormattedString (msgType, group, *pDD, level+1, globalDD));
				}
			}
		}
		return ret;
	}
	

	string Util::toFormattedString (const FIX::Message& msg, const FIX::DataDictionary &dd) {
		string ret;
		string msgType = msg.getHeader().getField (FIX::FIELD::MsgType);
		ret.append ("Header:\n").append (toFormattedString (msgType, msg.getHeader(), dd, 1, dd));
		ret.append ("Body:\n").append (toFormattedString (msgType, msg, dd, 1, dd));
		ret.append ("Trailer:\n").append (toFormattedString (msgType, msg.getTrailer(), dd, 1, dd));
		return ret;
	}
	

	bool Util::tooLate (const string& timestamp, int ms) {
		// Input format: yyyymmddhhmmss
		// Queremos saber se o timestamp � maior que a hora atual, descontada de "ms" milissegundos.
		// Se for, ent�o n�o est� atrasado.
		SYSTEMTIME st1, st2;
		FILETIME ft1, ft2;
		st1.wYear = atoi (timestamp.substr (0, 4).c_str());
		st1.wMonth = atoi (timestamp.substr (4, 2).c_str());
		st1.wDay = atoi (timestamp.substr (6, 2).c_str());
		st1.wHour = atoi (timestamp.substr (8, 2).c_str());
		st1.wMinute = atoi (timestamp.substr (10, 2).c_str());
		st1.wSecond = atoi (timestamp.substr (12, 2).c_str());
		st1.wMilliseconds = 0;
		GetSystemTime (&st2);
		SystemTimeToFileTime (&st1, &ft1);
		SystemTimeToFileTime (&st2, &ft2);
		LARGE_INTEGER li2;
		li2.LowPart = ft2.dwLowDateTime;
		li2.HighPart = ft2.dwHighDateTime;
		li2.QuadPart -= ms * 10000LL; // 1 ms = 1000 microssegundos = 1000 * (1000 / 100) nanossegundos
		ft2.dwLowDateTime = li2.LowPart;
		ft2.dwHighDateTime = li2.HighPart;
		return CompareFileTime (&ft1, &ft2) < 0;
	}
	
	/** 
	 * Efetua as seguintes normaliza��es: 
	 * - Elimina acentos e cedilhas dos nomes; 
	 * - Converte aspas duplas em simples; 
	 * - Converte algumas letras estrangeiras para seus equivalentes ASCII 
	 * (como � = eszet, convertido para ss)  
	 * - Converte os sinais de = para - 
	 * - Alguns caracteres s�o removidos: 
	 * -> os superiores a 255, 
	 * mesmo que possam ser representados por letras latinas normais 
	 * (como S, = U+015E = Latin Capital Letter S With Cedilla); 
	 * -> os caracteres de controle (exceto tab) 
	 * @param str A string a normalizar. 
	 * @return A string normalizada. 
	 */  
	string Util::filterNonAsciiChars (const string& str) {
	/*

		// Para evitar problemas com a codifica��o FAST (que pode ocorrer, por exemplo, 
		// com a mensagem News), iremos remover acentos e trocar alguns caracteres.
		// " !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~��������������������������������������������������������������������������������������������������������������������������������"
		// " !"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~ E 'f".++^%S<O Z  ''"".--~Ts>o zY !cL$Y|p"(a<--(-o+23'up.,10>113?AAAAAAACEEEEIIIIDNOOOOOxOUUUUYTsaaaaaaaceeeeiiiidnooooo/ouuuuyty"
		// "                                                                                                u  r . + o  E            M  e      = = a C    R  /    a     ///                 H             Hs                h             h "
		// "                                                                                                r    .                                 r )    )  -    r     424                                                                 "
	*/
		const static char FIRST_CHAR[] = 
		" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_"
		"`abcdefghijklmnopqrstuvwxyz{|}~ E 'f\".++^%S<O Z  ''\"\".--~Ts>o zY"
		" !cL$Y|p\"(a<--(-o+23'up.,10>113?AAAAAAACEEEEIIIIDNOOOOOxOUUUUYTs"
		"aaaaaaaceeeeiiiidnooooo/ouuuuyty";
		const static char SECOND_CHAR[] = 
		"                                                                "
		"                                u  r . + o  E            M  e   "
		"   = = a C    R  /    a     ///                 H             Hs"
		"                h             h ";
		const static char THIRD_CHAR[] = 
		"                                                                "
		"                                r    .                          "
		"       r )    )  -    r     424                                 "
		"                                ";
		string ret;
		ret.reserve (str.length() * 2); // 
		for (size_t i = 0, n = str.length(); i < n; ++i) {
			unsigned char ch = (unsigned char) str[i];
			if (ch == ' ' || ch == '\t') 
				ret.append (1, ' ');
			else if (ch >= ' ') {
				if (FIRST_CHAR[ch - ' '] != ' ') 
					ret.append (1, FIRST_CHAR[ch - ' ']);
				if (SECOND_CHAR[ch - ' '] != ' ')
					ret.append (1, SECOND_CHAR[ch - ' ']);
				if (THIRD_CHAR[ch - ' '] != ' ')
					ret.append (1, THIRD_CHAR[ch - ' ']);
			}
		}
		return ret;
	}
	
	string Util::join (const string& separator, const string& str1)
	{
		return str1;
	}
	
	string Util::join (const string& separator, const string& str1, const string& str2)
	{
		if (str1.empty())
			return str2;
		if (str2.empty())
			return str1;
		return str1 + separator + str2;
	}
	

	string Util::join (const string& separator, const string& str1, const string& str2, const string& str3)
	{
		string ret = str1;
		if (!str2.empty()) {
			if (ret.empty())
				ret = str2;
			else
				ret.append (separator).append (str2);
		}
		if (!str3.empty()) {
			if (ret.empty())
				ret = str3;
			else
				ret.append (separator).append (str3);
		}

		return ret;
	}
	

	string Util::join (const string& separator, const string& str1, const string& str2, const string& str3, const string& str4)
	{
		string ret = str1;
		if (!str2.empty()) {
			if (ret.empty())
				ret = str2;
			else
				ret.append (separator).append (str2);
		}
		if (!str3.empty()) {
			if (ret.empty())
				ret = str3;
			else
				ret.append (separator).append (str3);
		}
		if (!str4.empty()) {
			if (ret.empty())
				ret = str4;
			else
				ret.append (separator).append (str4);
		}

		return ret;
	}
	

	string Util::join (const string& separator, const string& str1, const string& str2, const string& str3, const string& str4, const string& str5)
	{
		string ret = str1;
		if (!str2.empty()) {
			if (ret.empty())
				ret = str2;
			else
				ret.append (separator).append (str2);
		}
		if (!str3.empty()) {
			if (ret.empty())
				ret = str3;
			else
				ret.append (separator).append (str3);
		}
		if (!str4.empty()) {
			if (ret.empty())
				ret = str4;
			else
				ret.append (separator).append (str4);
		}
		if (!str5.empty()) {
			if (ret.empty())
				ret = str5;
			else
				ret.append (separator).append (str5);
		}

		return ret;
	}
	
	bool Util::isSignaledTerminationEvent (HANDLE h)
	{
		DWORD dw = ::WaitForSingleObject (h, 0);
		return dw == WAIT_OBJECT_0 || dw == WAIT_ABANDONED_0 || dw == WAIT_FAILED;
	}

	
	string Util::getMatchingFieldValue (int tagNo, const string& serializedGroup, int occurrence)
	{
		string ret;
		string searchString = format ("\001" "%d=", tagNo);
		string::size_type pos = 0;
		int fieldsFound = 0;
		if (occurrence > 0)
		{
			while ((pos = serializedGroup.find (searchString, pos)) != string::npos) 
			{
				string::size_type startOfValuePos = pos + searchString.size();
				string::size_type endPos = serializedGroup.find ('\001', startOfValuePos);
				if (endPos != string::npos) 
				{
					fieldsFound++;
					if (fieldsFound == occurrence) 
					{
						ret.assign (serializedGroup, startOfValuePos, endPos - startOfValuePos);
						break;
					} 
					else
					{
						pos = endPos;
					}
				}
			}
		}
		return ret;
	}
	
	string Util::getFirstMatchingFieldValue (const string& serializedGroup, int firstTag, ...)
	{
		va_list arglist;
		va_start (arglist, firstTag);
		string ret;
		int tagNo = firstTag;
		do {
			string searchString = format ("\001" "%d=", tagNo);
			string::size_type pos = 0;
			int fieldsFound = 0;
			while ((pos = serializedGroup.find (searchString, pos)) != string::npos) 
			{
				string::size_type startOfValuePos = pos + searchString.size();
				string::size_type endPos = serializedGroup.find ('\001', startOfValuePos);
				if (endPos != string::npos) 
				{
					ret.assign (serializedGroup, startOfValuePos, endPos - startOfValuePos);
					return ret;
				}
			}
			tagNo = va_arg (arglist, int);
		} while (tagNo >= 0);
		va_end (arglist);
		return ret;
	}

};


