#include "StdAfx.h"
#include "ITCHDictionary.h"

namespace UMDFITCH 
{

	ITCHDictionary::ITCHDictionary(void)
	{
	}

	ITCHDictionary::ITCHDictionary(Config lConfig)
	{
		m_Config = lConfig;
		LoadITCHMessages();		
	}

	ITCHDictionary::~ITCHDictionary(void)
	{
	}

	void ITCHDictionary::LoadITCHMessages()
	{
		Util::parseXML ( m_Config.m_ItchDataDictionary , false, m_document);		

		LoadFields();
		LoadMessages();
	}

	void ITCHDictionary::LoadFields ()
	{		
		Util::DOMElements fieldElements = Util::evalXPath (m_document, L"/itch/fields/field");

		for (Util::DOMElements::iterator it = fieldElements.begin(); it != fieldElements.end(); ++it) 
		{
			FIELD field;
			field.Name = Util::getAttribute ((*it), L"name");
			field.Type = Util::getAttribute((*it),L"type");
			field.Length = atoi((Util::getAttribute((*it),L"length")).c_str());
			field.Description = Util::getAttribute((*it),L"description");
			
			m_Fields.insert(field);			
		}		
	}

	void ITCHDictionary::LoadMessages()
	{
		Util::DOMElements messageElements = Util::evalXPath(m_document, L"/itch/messages/message");

		for (Util::DOMElements::iterator it1 = messageElements.begin(); it1 != messageElements.end(); ++it1)
		{			
			shared_ptr<MESSAGE> p_message(new MESSAGE());

			p_message->Name = Util::getAttribute((*it1),L"name");
			p_message->Type = Util::getAttribute((*it1),L"type");
			p_message->Length = atoi ( (Util::getAttribute((*it1),L"length")).c_str() );
			
			Util::DOMElements messageFieldsElements = Util::childrenElements((*it1), "field");
			for ( Util::DOMElements::iterator itFields = messageFieldsElements.begin(); itFields != messageFieldsElements.end() ; itFields++ )
			{				
				shared_ptr<FIELD> p_msgField(new FIELD() );
				p_msgField->Name = Util::getAttribute((*itFields),L"name");
				p_msgField->Position = atoi( ( Util::getAttribute((*itFields),L"position") ).c_str() );				
				
				p_message->m_FieldsPtr->push_back(p_msgField);
			}

			m_Messages.insert(p_message);
		}
	}

};