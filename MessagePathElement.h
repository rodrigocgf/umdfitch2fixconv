#pragma once

namespace UMDFITCH 
{
	using std::string;

	class MessagePathElement
	{
		friend class MessagePathEnumerator;
	public:
		MessagePathElement(void);
		MessagePathElement(string rawPathElement);
		~MessagePathElement(void);

		string GetPathElement();
		string ToString();
		string m_selector;
	private:
		string m_rawPathElement;
		string m_pathElement;
		
	};

};