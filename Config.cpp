#include "StdAfx.h"
#include "Config.h"
#include "Util.h"
#include <io.h>

#include "BmfWin32/ILog.h"

namespace UMDFITCH 
{ 
	#define GETPLUGINCONFIG(_entryName_, _variable_, _level_) \
    if (!getPluginConfig (entryName = _entryName_, _variable_)) \
    { \
        errors.push_back (make_pair<string, int> (Util::format ("ITCH2FIX: BTSFIXGateway.xml: entry %s was not found for plugin instance %s",  \
            entryName.c_str(), pluginID.c_str()), _level_)); \
    }

	#define GETPLUGINCONFIGDEFAULT(_entryName_, _default_, _variable_, _level_) \
    if (!getPluginConfig (entryName = _entryName_, _default_, _variable_)) \
    { \
        errors.push_back (make_pair<string, int> (Util::format ("ITCH2FIX: BTSFIXGateway.xml: entry %s was not found for plugin instance %s",  \
            entryName.c_str(), pluginID.c_str()), _level_)); \
    }

	Config::Config(void)
	{
		
	}

	Config::~Config(void)
	{
	}

	vector<pair<string, int> > Config::loadConfiguration (BTS::IGatewayApplication * pApp_, const BTS::IGatewayPlugin * pPlugin_, const string& pluginID)
	{
		vector<pair<string, int> > errors;
		pApp = pApp_;
		pPlugin = pPlugin_;
		m_pluginID = pluginID;
		
		string entryName;

		if (!getPluginConfig (entryName = CONFIG::FIX50DATADICTIONARY, m_fIX50DataDictionary))
		{
			errors.push_back (make_pair<string, int>(	Util::format (	"ITCH2FIX: BTSFIXGateway.xml: entry %s was not found for plugin instance %s", 
																		entryName.c_str(), 
																		pluginID.c_str()
																	 ), 
														BmfWin32::LERROR)
													);
		} 
		else 
		{
			if (_access (m_fIX50DataDictionary.c_str(), 04) == -1) 
			{
				errors.push_back (make_pair<string, int> (	Util::format (	"ITCH2FIX: BTSFIXGateway.xml: file %s (%s) was not found or is unreadable for plugin instance %s", 
																			m_fIX50DataDictionary.c_str(), 
																			entryName.c_str(), 
																			pluginID.c_str()
																		 ), 
															BmfWin32::LERROR)
														 );
			}
		}
		
		GETPLUGINCONFIG(CONFIG::ITCHTOFIXCONVERTION, m_ItchToFixConvertionFile , BmfWin32::LERROR);
		GETPLUGINCONFIG(CONFIG::ITCHDATADICTIONARY, m_ItchDataDictionary , BmfWin32::LERROR);
		GETPLUGINCONFIG(CONFIG::NUMBEROFBROADCASTERS, m_iNumberOfBroadcasters, BmfWin32::LERROR);

		GETPLUGINCONFIG(CONFIG::ITCHTRANSPORT, m_strItchTransport, BmfWin32::LERROR);

		for (int i = 1; i <= m_iNumberOfBroadcasters; ++i) 
		{
			m_vecMDBroadcasterEntryNames.push_back(""); 
			//symbolRanges.push_back("");
			GETPLUGINCONFIG(Util::format ("%s%02d", CONFIG::MDBROADCASTERENTRYNAME, i), m_vecMDBroadcasterEntryNames[i-1], BmfWin32::LERROR);
			//GETPLUGINCONFIG(Util::format ("%s%02d", CONFIG::SYMBOLRANGE, i), symbolRanges[i-1], BmfWin32::LERROR);

			// Validando a sintaxe
			/*
			if ( ! boost::regex_match (symbolRanges[i-1], what, fullExpr)) 
			{
				errors.push_back (make_pair<string, int> (Util::format ("ITCH2FIX: BTSFIXGateway.xml: entry %s is not a range. Sample ranges: \"A-K\"; \"A-K,P,Q-Z\"; \"P\".",  
					entryName.c_str(), pluginID.c_str()), BmfWin32::LERROR));
			}
			*/
		}

		GETPLUGINCONFIG(CONFIG::ITCHIP, m_strItchIP, BmfWin32::LERROR);
		GETPLUGINCONFIG(CONFIG::ITCHPORT, m_strItchPort, BmfWin32::LERROR);
		GETPLUGINCONFIG(CONFIG::ITCHUSER, m_strUserName , BmfWin32::LERROR);
		GETPLUGINCONFIG(CONFIG::ITCHPASSWORD, m_strPassword, BmfWin32::LERROR);

		return errors;
	}


	bool Config::getPluginConfig (const string& configName, const string& defaultValue, string& value)
	{
		CComVariant vtConfig;  
		if( pApp->GetPluginConfig(pPlugin, configName.c_str(), vtConfig) && ( VT_BSTR == vtConfig.vt ) ) 
		{
			value = (const char*) (bstr_t (V_BSTR(&vtConfig))); 
			return true;
		} 
		else 
		{
			value = defaultValue;  
			return false;
		}
	}
	
	bool Config::getPluginConfig (const string& configName, string& value)
	{
		CComVariant vtConfig;  
		if( pApp->GetPluginConfig(pPlugin, configName.c_str(), vtConfig) && ( VT_BSTR == vtConfig.vt ) ) 
		{
			value = (const char*) (bstr_t (V_BSTR(&vtConfig)));  
			return true;
		} 
		else 
		{
			 return false;
		}
	}
	
	bool Config::getPluginConfig (const string& configName, int defaultValue, int& value)
	{
		CComVariant vtConfig;  
		if (pApp->GetPluginConfig(pPlugin,configName.c_str(), vtConfig) &&
			SUCCEEDED (vtConfig.ChangeType (VT_I4)))
		{
			 
			value = V_I4(&vtConfig);  
			return true;
		}
		else
		{
			value = defaultValue;  
			return false;
		}
	}
	
	bool Config::getPluginConfig (const string& configName, int& value)
	{
		CComVariant vtConfig;
		if (pApp->GetPluginConfig(pPlugin,configName.c_str(), vtConfig) &&
			SUCCEEDED (vtConfig.ChangeType (VT_I4)))
		{
			value = V_I4(&vtConfig);  
			return true;
		}
		else
		{
			return false;
		}
	}
	
	bool Config::getPluginConfig (const string& configName, bool defaultValue, bool& value)
	{
		CComVariant vtConfig;
		if (pApp->GetPluginConfig(pPlugin,configName.c_str(), vtConfig) &&
			SUCCEEDED (vtConfig.ChangeType (VT_BOOL)))
		{
			value = V_BOOL(&vtConfig) != VARIANT_FALSE;  
			return true;
		}
		else
		{
			value = defaultValue;  
			return false;
		}
	}
	
	bool Config::getPluginConfig (const string& configName, bool& value)
	{
		CComVariant vtConfig;
		if (pApp->GetPluginConfig(pPlugin,configName.c_str(), vtConfig) &&
			SUCCEEDED (vtConfig.ChangeType (VT_BOOL)))
		{
			value = V_BOOL(&vtConfig) != VARIANT_FALSE;  
			return true;
		}
		else
		{
			return false;
		}
	}

};