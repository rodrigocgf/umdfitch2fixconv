#include "StdAfx.h"
#include "MessagePathElement.h"

namespace UMDFITCH 
{

	MessagePathElement::MessagePathElement(void)
	{
	}

	MessagePathElement::MessagePathElement(string rawPathElement)
	{
		m_rawPathElement = rawPathElement;
		
		if ( m_rawPathElement.find_last_of("]") == -1 )
			m_pathElement = rawPathElement;
		else
		{
			int selectorStartPosition = m_rawPathElement.find_first_of('[');
			
			//Asserts that the selector is well formed
			//Debug.Assert(selectorStartPosition != -1);

			//Asserts that the selector delimiters actually contain a selector
			//Debug.Assert((selectorStartPosition + 1) != (_rawPathElement.Length - 1)); 

			m_pathElement = m_rawPathElement.substr(0, selectorStartPosition);
			m_selector = m_rawPathElement.substr(	selectorStartPosition + 1, m_rawPathElement.length() - selectorStartPosition - 2);
		}
	}

	MessagePathElement::~MessagePathElement(void)
	{
	}

	string MessagePathElement::GetPathElement()
	{
		return m_pathElement;
	}

	string MessagePathElement::ToString()
	{
		if (m_selector.length() == 0)
			return m_pathElement;
		return m_pathElement + "[" + m_selector + "]";
	}


}