#pragma once

#include "IUMDFITCH2FIXConverter.hpp"
#include "Config.h"
//#include "SecurityList.hpp"
//#include "RangeSelector.hpp"
#include "Extensions/SDK/Interfaces/UMDFSecurityDataSubscriber.h"
#include "Extensions/SDK/Interfaces/UMDFSecurityDataProvider.h"
#include "INASDAQReceiver.h"
#include "ReceiverFactory.h"
#include "ITCHCracker.h"
#include "FIXAssembler.h"
#include "Convertions.h"
#include "Util.h"



namespace UMDFITCH 
{ 
	namespace Plugin 
	{
		using std::string;
		using std::vector;
		using boost::unordered_map;
		using boost::shared_ptr;


		typedef unordered_map<string, HANDLE> MapChannelToHandle;

		class UMDFITCH2FIXConverter :  public IUMDFITCH2FIXConverter, public UMDF::IUMDFSecuritySubscriber 
		{
		public:
			UMDFITCH2FIXConverter(LPCTSTR strPluginID);    
			virtual ~UMDFITCH2FIXConverter();

			public:
			virtual bool OnLoad();
			virtual bool OnStart();
			virtual void OnStop();
			virtual void OnLogon(const FIX::SessionID &);
			virtual void OnLogout(const FIX::SessionID &);
			virtual LPCTSTR GetPluginID() const;
			virtual UINT OnMessage(const FIX::Message &,const BTS::Sender &);
			/// <summary>
			/// Security List Listener - called by the SecurityList Plugin each time a security is added/changed/removed
			/// </summary>
			virtual bool OnSecurities(const vector<IUMDFSecuritySubscriber::SecurityData>& securities, const string& channel);

			void ExecuteConvertion(unsigned char * buffer, int size);
		private:

			/// <summary>
			/// The Plugin ID (e.g., "PI_UMDFITCH2FIXCONVERTER_01"
			/// </summary>
			string m_pluginID;

			/// <summary>
			/// UMDFSecurityProvider plugin entry point
			/// </summary>
			//UMDF::IUMDFSecurityProvider* m_securityProviderEntryPointPtr;

			/// <summary>
			/// Configuration
			/// </summary>
			Config m_config;

			/// <summary>
			/// FIX 5.0 Dictionary
			/// </summary>
			FIX::DataDictionary m_fIX50DataDictionary;

			/// <summary>
			/// UMDFMDBroadcaster plugin entry point
			/// </summary>
			vector<BTS::IMessageEntryPoint*> m_vecMDBroadcasterEntryPoint;

			/// <summary>
			/// Security List
			///</summary>
			//UMDFITCH::Converter::SecurityListPtr m_securityList;

			/// <summary>
			/// Shutdown event
			/// </summary>
			HANDLE m_shutdownEvent;

			/// <summary>
			/// Receiver Factory
			/// </summary>
			shared_ptr<INASDAQReceiver> receiverPtr;

			/// <summary>
			/// ITCH Cracker
			/// </summary>
			shared_ptr<ITCHCracker> ITCHCrackerPtr;

			shared_ptr<Convertions> convertionsPtr;

			shared_ptr<FIXAssembler> FIXAssemblerPtr;
		};

	}; 
};


