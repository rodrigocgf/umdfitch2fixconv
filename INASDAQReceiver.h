#pragma once

namespace UMDFITCH 
{ 
	 class INASDAQReceiver
	{
	public:
		virtual void Start() = 0;
		virtual void Stop() = 0;
		virtual void RequestMessage(long sequenceNumber) = 0;
		virtual void ParseMessage() = 0;
	};

};