#include "StdAfx.h"
#include "MessagePath.h"

namespace UMDFITCH 
{	
	MessagePathEnumerator::MessagePathEnumerator()
	{	
		m_pathAsNamespaceBuilder.clear();
		m_index = -1;
	}	

	void MessagePathEnumerator::SetParent(MessagePath * p_messagePath)
	{
		m_pMessagePath = p_messagePath;
	}

	MessagePathEnumerator::~MessagePathEnumerator(void)
	{
	}

	shared_ptr<MessagePathElement> MessagePathEnumerator::Current()
	{		
		shared_ptr<MessagePathElement>  p_Ret;		
		p_Ret = m_pMessagePath->m_pathElements[m_index];

		return p_Ret;
	}

	string MessagePathEnumerator::GetUpdatedPathElement()
	{		
		string path;
        
		for(int i = 0; i < m_pMessagePath->m_pathElements.size(); i++)
		{
            path.append(m_pMessagePath->m_pathElements[i]->ToString() );

            if ( i < m_pMessagePath->m_pathElements.size() - 1)
                path.append("/");
        }
        return path;
	}

	shared_ptr<MessagePathEnumerator> MessagePathEnumerator::Clone()
	{
		shared_ptr<MessagePathEnumerator> p_clone;
		p_clone.reset( new MessagePathEnumerator() );
		p_clone->SetParent(m_pMessagePath);
        p_clone->m_index = m_index;
        p_clone->m_pathAsNamespaceBuilder = m_pathAsNamespaceBuilder;
        
		return p_clone;
	}

	void MessagePathEnumerator::Reset()
	{
		m_index = -1;
		m_pathAsNamespaceBuilder.clear();
	}

	bool MessagePathEnumerator::MoveNext()
	{		
		if (m_index < (int)(m_pMessagePath->m_pathElements.size() - 1) )		
		{
			m_index++;
            if ( m_pMessagePath->m_pathElements[m_index]->m_selector.length() > 0)
            {
				if (m_pathAsNamespaceBuilder.length() != 0)
                    m_pathAsNamespaceBuilder.append(".");

                m_pathAsNamespaceBuilder.append( m_pMessagePath->m_pathElements[m_index]->GetPathElement());
            }
			return true;
		}
		else
		{
			return false;
		}
	}

	bool MessagePathEnumerator::HasMoreElements()
	{
		if ( m_index + 1 <= m_pMessagePath->m_pathElements.size() - 1)
			return true;
		else
			return false;
	}

	string MessagePathEnumerator::GetCleanNamespace()
	{
		return m_pathAsNamespaceBuilder;
	}

}