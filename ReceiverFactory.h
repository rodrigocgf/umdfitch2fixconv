#pragma once

#include "INASDAQReceiver.h"
#include "Config.h"
#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string/case_conv.hpp>


namespace UMDFITCH 
{
	using boost::shared_ptr;

	class ReceiverFactory 
	{
	private:
		ReceiverFactory(void) {}
		~ReceiverFactory(void) {}
	public:		
		static shared_ptr<INASDAQReceiver> CreateReceiver(Config lConfig, void * parent);
		
	};
};