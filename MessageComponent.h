#pragma once

#pragma warning (push)
#pragma warning (disable : 4146 4267)
//#include "FIXDictionary.h"
#include "Util.h"
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include <quickfix/Values.h>
#include <quickfix/Fields.h>
#include <quickfix/Message.h>
#pragma warning (pop)

namespace UMDFITCH 
{
	using std::string;
	using std::map;
	using std::list;
	using boost::shared_ptr;
	using boost::multi_index_container;
	using namespace boost::multi_index;	
	
	enum ComponentType
	{
        TYPE_HEADER=0,
        TYPE_TRAILER,
		TYPE_MESSAGE,
		TYPE_COMPONENTBLOCK,
		TYPE_REPEATINGGROUP,
		TYPE_REPEATINGGROUPINSTANCE,
		TYPE_FIELD
	};	

	class MessageComponent
	{
	public:
		MessageComponent(void);
		MessageComponent(string name, ComponentType type);
		~MessageComponent(void);

		string								m_Name;
		ComponentType						m_Type;		
		FIELD								m_Value;

		void								AddChild(string childKey, shared_ptr<MessageComponent> p_child);		
		void								RemoveChild(string childKey);		
		bool								HasChildren();
		bool								HasChild(string childKey);
		shared_ptr<MessageComponent>		GetChild(string childKey);		
		list<shared_ptr<MessageComponent> > GetChildren();
		map<string, shared_ptr<MessageComponent> > GetKeyedChildren();
		list<shared_ptr<MessageComponent> >			m_fieldOrdering;
	private:

		map<string, shared_ptr<MessageComponent> >	m_children;		
		
		
	};

};