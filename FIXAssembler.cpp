#include "StdAfx.h"
#include "FIXAssembler.h"
#include <quickfix/fix50sp2/MarketDataIncrementalRefresh.h>
#include <quickfix/fix50sp2/SecurityList.h>
#include<boost/tokenizer.hpp>
#include "BmfWin32/ILog.h"

namespace UMDFITCH 
{
	using namespace boost;

	const int SEPARATOR_LENGTH = 1;
    const int TERMINATOR_LENGTH = 1;
    const int MSGTYPE_TAG_LENGTH = 2;

	FIXAssembler::FIXAssembler(void)
	{
	}

	FIXAssembler::FIXAssembler(Config lConfig)
	{
		m_Config = lConfig;
		m_dMsgSeqNum = 0;

		// Load FIX messages from FIX50SP2.UMDF.xml		
		FIXDictionaryPtr.reset( new FIXDictionary(m_Config) );

		// Loading the FIX 5.0 Data Dictionary
		try
		{
			m_fIX50DataDictionary.readFromURL(m_Config.m_fIX50DataDictionary);
		}
		catch (FIX::ConfigError& configerror)
		{
			GW_ERROR_FORMAT(_T("%s(%s) - Error loading FIX5.0 data dictionary [%s]: %s."), 
				__FUNCTION__ % "UMDFITCH2FIXConv"//GetPluginID() 
				% m_Config.m_fIX50DataDictionary 
				% configerror.what());			
		}
	}

	FIXAssembler::~FIXAssembler(void)
	{
	}

	void FIXAssembler::SetMessageType(string msgType)
	{
		m_MessageType = msgType;

		map<string, shared_ptr<NODE> >::iterator itMessage = FIXDictionaryPtr->m_DictMessages.find(msgType);
		if ( itMessage != FIXDictionaryPtr->m_DictMessages.end() )
		{
			shared_ptr<NODE> nodeMsg = itMessage->second;
			string messageName = nodeMsg->GetAttribute("name");		

			m_bodyPtr.reset( new MessageComponent(messageName, TYPE_MESSAGE) );
		}
	}

	shared_ptr<FIX::Message> FIXAssembler::Assemble()
	{	
		m_headerPtr.reset( new MessageComponent("header", TYPE_HEADER ) );        
        m_trailerPtr.reset( new MessageComponent("trailer", TYPE_TRAILER) );        

		BuildHeaderAndTrailer();
		string strMessage = CompileToString();
		
		if ( strMessage.length() > 0 )
		{
			GW_TRACE_FORMAT ("ASSEMBLED MESSAGE : [%s] ", strMessage.c_str());

			try
			{	
				shared_ptr<FIX::Message> messagePtr;
				messagePtr.reset( new FIX::Message(strMessage,m_fIX50DataDictionary) );
				
				return messagePtr;
			}
			catch( FIX::InvalidMessage & ex )
			{
				
				GW_TRACE_FORMAT("Invalid Message Error : %s" , ex.what() );
				shared_ptr<FIX::Message> messagePtr( new FIX::Message());
				return messagePtr;
			}
			
		} else {
			shared_ptr<FIX::Message> messagePtr( new FIX::Message());
			return messagePtr;
		}
		
	}		

	void FIXAssembler::BuildHeaderAndTrailer()
	{	
		char szMsgSeqNum[20];

		string fixString;
		fixString.append("FIXT.");
		fixString.append(FIXDictionaryPtr->GetMajorVersion());
		fixString.append(".");
		fixString.append(FIXDictionaryPtr->GetMinorVersion());
		FIELD beginString("BeginString",fixString);		
		SetField(beginString);

		memset(szMsgSeqNum,0x00,sizeof(szMsgSeqNum));
		sprintf(szMsgSeqNum,"%.0f",m_dMsgSeqNum);
		FIELD msgSeqNum("MsgSeqNum",szMsgSeqNum);
		SetField(msgSeqNum);
		m_dMsgSeqNum += (double)1;

		FIELD bodyLength("BodyLength", "");
		SetField(bodyLength);		

		FIELD msgType("MsgType",m_MessageType);
		SetField(msgType);

		FIELD checkSum("CheckSum","");
		SetField(checkSum);
	}

	void FIXAssembler::SetField(FIELD field)
	{		
		MessagePath fieldLocation(field.Name);
        
		string pathElement = fieldLocation.GetLastPathElement()->GetPathElement();
		int fieldNum = FIXDictionaryPtr->GetFieldNumberByName(pathElement);
		char szFieldNum[10];
		memset(szFieldNum,0x00,sizeof(szFieldNum));
		sprintf(szFieldNum,"%d", fieldNum);
		field.Name = szFieldNum;

        if (FIXDictionaryPtr->IsHeaderField(fieldLocation.GetLastPathElement()->GetPathElement()))
            SetField(field, m_headerPtr, fieldLocation.GetEnumerator());
        else if (FIXDictionaryPtr->IsTrailerField(fieldLocation.GetLastPathElement()->GetPathElement()))
            SetField(field, m_trailerPtr, fieldLocation.GetEnumerator());
        else
            SetField(field, m_bodyPtr, fieldLocation.GetEnumerator());
	}

	void FIXAssembler::SetField(FIELD field, shared_ptr<MessageComponent> & p_messageComponent, shared_ptr<MessagePathEnumerator> & p_fieldPathEnumerator)
	{
		p_fieldPathEnumerator->MoveNext();

		shared_ptr<MessagePathElement> p_pathElement = p_fieldPathEnumerator->Current();
		
		if (!p_fieldPathEnumerator->HasMoreElements())
		{
			shared_ptr<MessageComponent> p_fieldComponent;
			p_fieldComponent.reset( new MessageComponent(p_pathElement->GetPathElement(), TYPE_FIELD ) );
			p_fieldComponent->m_Value = field;
			
			if (p_messageComponent->HasChild(p_pathElement->GetPathElement()))
			{
				shared_ptr<MessageComponent> tmp = p_messageComponent->GetChild(p_pathElement->GetPathElement());
				p_messageComponent->RemoveChild(p_pathElement->GetPathElement());
				//m_checksum.RemoveFieldFromChecksum(tmp->m_Value);
				m_iBodyLength -= CalculateFieldLength(tmp->m_Value);
			}
			p_messageComponent->AddChild(p_pathElement->GetPathElement(), p_fieldComponent);
			//m_checksum.AddFieldToChecksum(field);
			m_iBodyLength += CalculateFieldLength(field);
		}
		else
		{			
			if (p_pathElement->m_selector.length() == 0)				
			{
				shared_ptr<MessageComponent> componentBlock;
				if (p_messageComponent->HasChild(p_pathElement->GetPathElement()))
					componentBlock =  p_messageComponent->GetChild(p_pathElement->GetPathElement() );
				else
				{
					componentBlock.reset( new MessageComponent(p_pathElement->GetPathElement(), TYPE_COMPONENTBLOCK) );
					p_messageComponent->AddChild(p_pathElement->GetPathElement(), componentBlock);
				}
				SetField(field, componentBlock, p_fieldPathEnumerator);
			}			
			else
			{
				shared_ptr<MessageComponent> repeatingGroup;
				
				if (p_messageComponent->HasChild(p_pathElement->GetPathElement()))
				{
					repeatingGroup = p_messageComponent->GetChild(p_pathElement->GetPathElement());
				} else {
					repeatingGroup.reset( new MessageComponent(p_pathElement->GetPathElement(), TYPE_REPEATINGGROUP) );
					p_messageComponent->AddChild(p_pathElement->GetPathElement(), repeatingGroup);
				} 				
				

				char szLastGroupInstance[10];
				memset(szLastGroupInstance,0x00,sizeof(szLastGroupInstance));

				if (p_pathElement->m_selector == "new")
				{					
					sprintf(szLastGroupInstance,"%d",repeatingGroup->GetChildren().size());
					string lastGroupInstance = szLastGroupInstance;

					shared_ptr<MessageComponent> repeatingGroupInstance;
					repeatingGroupInstance.reset( new MessageComponent(lastGroupInstance, TYPE_REPEATINGGROUPINSTANCE) );
					repeatingGroup->AddChild(lastGroupInstance, repeatingGroupInstance);
					SetField(field, repeatingGroupInstance, p_fieldPathEnumerator);
				} else if (p_pathElement->m_selector == "last") {
					
					sprintf(szLastGroupInstance,"%d",(repeatingGroup->GetChildren().size() - 1));
					string lastGroupInstance = szLastGroupInstance;
					
					shared_ptr<MessageComponent> repeatingGroupInstance = repeatingGroup->GetChild(lastGroupInstance);
					SetField(field, repeatingGroupInstance, p_fieldPathEnumerator);
				} else {					
					int instanceIndex = atoi( p_pathElement->m_selector.c_str() ) - 1;
					char szInstanceIndex[10];
					memset(szInstanceIndex,0x00,sizeof(szInstanceIndex));
					sprintf(szInstanceIndex,"%d",instanceIndex);
					string strInstanceIndex = szInstanceIndex;

					shared_ptr<MessageComponent> repeatingGroupInstance = repeatingGroup->GetChild(strInstanceIndex);
					if (repeatingGroupInstance != NULL)
						SetField(field, repeatingGroupInstance, p_fieldPathEnumerator);					
				}
			}
		}
	}

	int FIXAssembler::CalculateFieldLength(FIELD field)
	{
		return (int)(field.Name.length()) + SEPARATOR_LENGTH + (int)(field.Value.length()) + TERMINATOR_LENGTH;		
	}

	string FIXAssembler::FieldToString(FIELD field)
	{		
		string strRet;

		strRet.append(field.Name);
		strRet.append(string(1,SEPARATOR));
		strRet.append(field.Value);
		strRet.append(string(1,TERMINATOR));

		return strRet;
	}

	string FIXAssembler::CompileToString()
	{
		string strTime;
		SYSTEMTIME localTime;

		if ( m_bodyPtr != NULL ) // REVIEW THIS POINT !!!
		{
			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- ORDER HEADER : %s"),strTime);

			FIXDictionaryPtr->SetMessageOrderings(m_headerPtr);	

			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- ORDER BODY : %s"),strTime);

			FIXDictionaryPtr->SetMessageOrderings(m_bodyPtr);		

			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- ORDER TRAILER : %s"),strTime);

			FIXDictionaryPtr->SetMessageOrderings(m_trailerPtr);		

			string messagePayload;

			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- GET PAYLOAD HEADER : %s"),strTime);

			GetMessagePayload(m_headerPtr, messagePayload);

			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- GET PAYLOAD BODY : %s"),strTime);

			GetMessagePayload(m_bodyPtr, messagePayload);

			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- GET PAYLOAD TRAILER : %s"),strTime);

			GetMessagePayload(m_trailerPtr, messagePayload);

			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- FILL BODY LENGTH : %s"),strTime);

			FillBodyLength(messagePayload);

			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- CALCULATE CHECKSUM : %s"),strTime);

			CalculateChecksum(messagePayload);					
		
			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- END ASSEMBLE : %s"),strTime);

			return messagePayload;
		}
		else
			return "";
	}

	void FIXAssembler::FillBodyLength(string &message)
	{
		int  iBeginPosition, iEndPosition, iBodyLength;
		char szFindBegin [] =		{0x01,'3','5','=',0x00};
		char szFindEnd [] =			{0x01,'1','0','=',0x00};		
		char szFindBodyLength [] =	{0x01,'9','=',0x00};

		iBeginPosition = message.find(szFindBegin);
		iEndPosition = message.find(szFindEnd);
		iBodyLength = iEndPosition - iBeginPosition;
		char szBodyLength[20];
		memset(szBodyLength ,0x00,sizeof(szBodyLength));
		sprintf_s(szBodyLength,"%d",iBodyLength);
		
		int iBodyLengthPos = message.find(szFindBodyLength);
		message.insert((iBodyLengthPos + 3), szBodyLength);
	}

	void FIXAssembler::CalculateChecksum(string &message)
	{
		string strTime;
		SYSTEMTIME localTime;

		char szFindChecksum [] = {0x01,'1','0','=',0x00};
		int iChecksumPos = message.find(szFindChecksum);

		string strMsgToCalc = message.substr(0, iChecksumPos + 1);
		char * szMessage = (char *)strMsgToCalc.c_str();
		long bufLen = strlen(szMessage);


		long idx;
		unsigned int cks;
		char szCheckSum[4];
		memset(szCheckSum , 0x00,sizeof(szCheckSum) );		
	
		for( idx = 0L, cks = 0; idx < bufLen; cks += (unsigned int)szMessage[ idx++ ] );
		sprintf( szCheckSum, "%03d", (unsigned int)( cks % 256 ) );
		

		//string checksum = m_checksum.GetChecksum();
		
		message.insert((iChecksumPos + 4), szCheckSum);

		::GetLocalTime(&localTime);
		strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
		GW_INFO_FORMAT(_T(" ---- CALCULATE CHECKSUM 3 : %s"),strTime);
	}

	void FIXAssembler::GetMessagePayload(shared_ptr<MessageComponent> messageElementPtr, string & message)
	{
		if ( messageElementPtr != NULL )
		{
			if (messageElementPtr->m_Type == TYPE_FIELD )
				message.append(FieldToString(messageElementPtr->m_Value));
			else
			{
				if (messageElementPtr->m_Type == TYPE_REPEATINGGROUP)
				{
					FIELD repeatingGroupCounter;
					
					map<string, shared_ptr<NODE> >::iterator itField = FIXDictionaryPtr->m_DictFields.find(messageElementPtr->m_Name);
					shared_ptr<NODE> nodeField = itField->second;
					repeatingGroupCounter.Name = nodeField->GetAttribute("number");

					char szSize[10];
					memset(szSize,0x00,sizeof(szSize));
					sprintf_s(szSize,"%d", messageElementPtr->GetChildren().size() );
					repeatingGroupCounter.Value = szSize;				

					//m_checksum.AddFieldToChecksum(repeatingGroupCounter);
					m_iBodyLength += CalculateFieldLength(repeatingGroupCounter);
					message.append(FieldToString(repeatingGroupCounter));
				}

				for (	list<shared_ptr<MessageComponent> >::iterator itChildComponent = messageElementPtr->m_fieldOrdering.begin() ; 
						itChildComponent != messageElementPtr->m_fieldOrdering.end();
						itChildComponent++ )
				{
					shared_ptr<MessageComponent> loopMsgCompPtr = (*itChildComponent);
					GetMessagePayload(loopMsgCompPtr, message);
				}				
			}
		}
	}	

};