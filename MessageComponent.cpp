#include "StdAfx.h"
#include "MessageComponent.h"

namespace UMDFITCH 
{

	MessageComponent::MessageComponent(void)
	{
	}

	MessageComponent::MessageComponent(string name, ComponentType type)
	{
		m_Name = name;
		m_Type = type;
	}

	MessageComponent::~MessageComponent(void)
	{
	}

	list<shared_ptr<MessageComponent> > MessageComponent::GetChildren()
	{
		list< shared_ptr<MessageComponent> > retList;

		for ( map<string, shared_ptr<MessageComponent> >::iterator itMap = m_children.begin() ; itMap != m_children.end(); itMap++)
		{
			shared_ptr<MessageComponent> pComp = itMap->second;
			retList.push_back(pComp);
		}

		return retList;
	}

	map<string, shared_ptr<MessageComponent> > MessageComponent::GetKeyedChildren()
	{
		return m_children;
	}

	void MessageComponent::AddChild(string childKey, shared_ptr<MessageComponent> p_child)
	{
		m_children.insert(std::pair<string,shared_ptr<MessageComponent> >(childKey,p_child) );
	}

	void MessageComponent::RemoveChild(string childKey)
	{		
		map< string , shared_ptr<MessageComponent> >::iterator itMap = m_children.find(childKey);
		if ( itMap != m_children.end() )
			m_children.erase(itMap);		
	}

	shared_ptr<MessageComponent> MessageComponent::GetChild(string childKey)
	{
		return m_children[childKey];
	}

	bool MessageComponent::HasChildren()
	{
		return m_children.size() != 0 ;
	}

	bool MessageComponent::HasChild(string childKey)
	{
		if ( m_children.size() > 0 )
			return (m_children.find(childKey) != m_children.end() ? true : false);
		else
			return false;		
	}
}