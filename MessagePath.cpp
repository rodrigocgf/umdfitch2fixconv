#include "StdAfx.h"
#include "MessagePath.h"
#include<boost/tokenizer.hpp>


namespace UMDFITCH 
{
	using namespace boost;
	

	MessagePath::MessagePath(string rawPath)
	{
		m_rawPath = rawPath;		
		
		boost::char_separator<char> sepPath(" /");
		
		boost::tokenizer<boost::char_separator<char> > tokens(rawPath, sepPath);
		vector<string> splittedPath( tokens.begin() , tokens.end());
		
		int pathSize = splittedPath.size();
		
		m_pathElements.resize(pathSize);
        
		for(int i = 0; i < m_pathElements.size() ; i++)
			m_pathElements[i].reset(  new MessagePathElement(splittedPath[i]) );
	}


	MessagePath::~MessagePath(void)
	{
	}

	shared_ptr<MessagePathEnumerator> MessagePath::GetEnumerator()
	{
		shared_ptr<MessagePathEnumerator> pNewMessagePathEnumerator;
		pNewMessagePathEnumerator.reset( new MessagePathEnumerator() );		
		pNewMessagePathEnumerator->SetParent(this );
		
		return pNewMessagePathEnumerator;
	}

	shared_ptr<MessagePathElement> MessagePath::GetLastPathElement()
	{
		shared_ptr<MessagePathElement> pMsgElem = m_pathElements[m_pathElements.size() - 1];
		return pMsgElem;
	}


}