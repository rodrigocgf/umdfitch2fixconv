#include "StdAfx.h"
#include "ReceiverFactory.h"
#include "SoupBinTCP.h"
#include "MoldUDP64.h"

namespace UMDFITCH 
{
	using namespace boost::algorithm;

	shared_ptr<INASDAQReceiver> ReceiverFactory::CreateReceiver(Config lConfig, void * parent)
	{
		string lUpperConfig = to_upper_copy(lConfig.m_strItchTransport);

		shared_ptr<INASDAQReceiver> p_receiver;

		if ( lUpperConfig == "SOUPBINTCP" )
		{
			p_receiver =  (shared_ptr<INASDAQReceiver>) new SoupBinTCP(lConfig , parent);
		} else if ( lUpperConfig == "MOLDUDP64" ) {
			p_receiver =  (shared_ptr<INASDAQReceiver>) new MoldUDP64();
		} 

		return p_receiver;

	}
}