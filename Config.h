#pragma once
#include <string>
#include <vector>
#include <utility>
#pragma warning (push)
#pragma warning (disable : 4146 4267)
#include "Extensions/SDK/Interfaces/GatewayPlugin.h"
#pragma warning (pop)

namespace UMDFITCH 
{
	using std::string;
	using std::vector;
	using std::pair;
	using std::make_pair;

	namespace CONFIG
	{
		const char* const ATTACHDEBUGGER = "AttachDebugger";
		const char* const FIX50DATADICTIONARY = "FIX50DataDictionary";
		const char* const DEBUGFIX = "DebugFIX";
		const char* const ITCHDATADICTIONARY = "ITCHDataDictionary";		
		const char* const ITCHTOFIXCONVERTION = "ITCH_TO_FIX_Convertion";
		//const char* const DEBUGRLC = "DebugRLC";
		//const char* const DEBUGPRINTALLRLCMESSAGES = "DebugPrintAllRLCMessages";
		const char* const DEBUGPRINTFIXMESSAGEDETAILS = "DebugPrintFIXMessageDetails";
		const char* const ITCHPORT = "ITCHPort";
		const char* const ITCHIP = "ITCHIP";
		const char* const ITCHUSER = "ITCHUser";
		const char* const ITCHPASSWORD = "ITCHPassword";
		const char* const HUBTIMEOUT = "HubTimeout";
		const char* const HUBMSGTIMEOUT = "HubMsgTimeout";
		const char* const HUBSYNCHRONIZE = "HubSynchronize";
		const char* const HUBSYNCHRONIZETIMEOUT = "HubSynchronizeTimeout";
		const char* const SECURITYPROVIDERENTRYNAME = "SecurityProviderEntryName";
		const char* const CHANNEL = "Channel";
		const char* const NUMBEROFBROADCASTERS = "NumberOfBroadcasters";
		const char* const MDBROADCASTERENTRYNAME = "MDBroadcasterEntryName";
		const char* const ITCHTRANSPORT = "ITCHTransport";
		const char* const SYMBOLRANGE = "SymbolRange";
		const char* const USESECURITYLIST = "UseSecurityList";
		const char* const SECURITYLISTFILE = "SecurityListFile";
		//const char* const RLCMESSAGEFILE = "RLCMessageFile";
		//const char* const RLCALLOWEDMESSAGES = "RLCAllowedMessages";
		//const char* const RLCLOGFILE = "RLCLogFile";
		//const char* const RLCLOGFILEMAXSIZE = "RLCLogFileMaxSize";
		const char* const IGNOREDSECURITIES = "IgnoredSecurities";
		const char* const ALLOWEDSECURITIES = "AllowedSecurities";
		const char* const DELAYBETWEENPACKETS = "DelayBetweenPackets";
	};

	class Config
	{
	public:			

		Config(void);
		~Config(void);
		/// <returns> 
		/// Each pair has an error message and a severity code. 
		/// Currently: 
		///				0 = error, 1 = warning, 2 = info, 3 = debug, 4 = trace
		/// </returns>
		vector<pair<string, int> > loadConfiguration (BTS::IGatewayApplication *pApp, const BTS::IGatewayPlugin *pPlugin, const string& pluginID);
	private:
		private:
		///<param name="configName">Configuration entry name</param>
		///<param name="defaultValue">The default value</param>
		///<param name="value">(output)The retrieved value, if found, or the default value, if not found</param>
		///<returns>true if the value was found and the type is string</returns>
		bool getPluginConfig (const std::string& configName, const std::string& defaultValue, std::string& value);
		///<param name="configName">Configuration entry name</param>
		///<param name="value">(output)The retrieved value, if found. If not found, it's not touched</param>
		///<returns>true if the value was found and the type is string</returns>
		bool getPluginConfig (const std::string& configName, std::string& value);
		///<param name="configName">Configuration entry name</param>
		///<param name="defaultValue">The default value</param>
		///<param name="value">(output)The retrieved value, if found, or the default value, if not found</param>
		///<returns>true if the value was found and the type is string</returns>
		bool getPluginConfig (const std::string& configName, int defaultValue, int& value);
		///<param name="configName">Configuration entry name</param>
		///<param name="value">(output)The retrieved value, if found. If not found, it's not touched</param>
		///<returns>true if the value was found and the type is integer</returns>
		bool getPluginConfig (const std::string& configName, int& value);
		///<param name="configName">Configuration entry name</param>
		///<param name="defaultValue">The default value</param>
		///<param name="value">(output)The retrieved value, if found, or the default value, if not found</param>
		///<returns>true if the value was found and the type is integer (0 = false, !=0 = true) or boolean</returns>
		bool getPluginConfig (const std::string& configName, bool defaultValue, bool& value);
		///<param name="configName">Configuration entry name</param>
		///<param name="value">(output)The retrieved value, if found. If not found, it's not touched</param>
		///<returns>true if the value was found and the type is integer (0 = false, !=0 = true) or boolean</returns>
		bool getPluginConfig (const std::string& configName, bool& value);
		
		
		string						m_pluginID;
		BTS::IGatewayApplication *	pApp;
		const BTS::IGatewayPlugin *	pPlugin;
	public:
		string						m_channel;
		string						m_fIX50DataDictionary;
		string						m_ItchDataDictionary;
		string						m_ItchToFixConvertionFile;
		vector<string>				m_vecMDBroadcasterEntryNames;
		int							m_iNumberOfBroadcasters;
		string						m_strItchTransport;
		string						m_strItchIP;
		string						m_strItchPort;
		string						m_strUserName;
		string						m_strPassword;
	};


};