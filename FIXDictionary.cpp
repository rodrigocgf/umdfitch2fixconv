#include "StdAfx.h"
#include "FIXDictionary.h"

namespace UMDFITCH 
{
	FIXDictionary::FIXDictionary(void)
	{
	}

	FIXDictionary::FIXDictionary(Config lConfig)
	{
		m_Config = lConfig;
		LoadFIXMessages();		
	}

	FIXDictionary::~FIXDictionary(void)
	{
	}
	
	void FIXDictionary::LoadFIXMessages()
	{
		Util::parseXML ( m_Config.m_fIX50DataDictionary , false, m_document);		
		
		LoadVersion();
		LoadHeader();
		LoadTrailer();
		LoadFields(L"/fix/fields/field");
		LoadComponents(L"/fix/components/component");
		LoadMessages(L"/fix/messages/message");
	}
	

	void FIXDictionary::LoadVersion()
	{
		Util::DOMElements versionElements = Util::evalXPath (m_document, L"/fix");
		
		Util::DOMElements::iterator it = versionElements.begin();

		if ( it !=  versionElements.end() )
		{
			m_MajorVersion = Util::getAttribute ((*it), L"major");
			m_MinorVersion = Util::getAttribute ((*it), L"minor");
		}
	}

	void FIXDictionary::LoadHeader()
	{
		Util::DOMElements fieldElements = Util::evalXPath (m_document, L"/fix/header/field");

		m_NodeHeaderPtr.reset(new NODE() );
		m_NodeHeaderPtr->Name = "header";

		for (Util::DOMElements::iterator it = fieldElements.begin(); it != fieldElements.end(); ++it) 
		{
			shared_ptr<NODE> lNodePtr(new NODE());
			ATTRIBUTE attr("name", Util::getAttribute ((*it), L"name") );			
			lNodePtr->m_AttributesPtr->push_back(attr);
			m_NodeHeaderPtr->m_ChildNodesPtr->push_back(lNodePtr);			
		}
	}
	
	void FIXDictionary::LoadTrailer()
	{
		Util::DOMElements fieldElements = Util::evalXPath (m_document, L"/fix/trailer/field");

		m_NodeTrailerPtr.reset(new NODE());
		m_NodeTrailerPtr->Name = "trailer";

		for (Util::DOMElements::iterator it = fieldElements.begin(); it != fieldElements.end(); ++it) 
		{			
			shared_ptr<NODE> lNodePtr(new NODE());
			ATTRIBUTE attr("name", Util::getAttribute ((*it), L"name") );			
			lNodePtr->m_AttributesPtr->push_back(attr);
			m_NodeTrailerPtr->m_ChildNodesPtr->push_back(lNodePtr);
		}
	}

	void FIXDictionary::LoadFields (wstring messagesPath)
	{
		Util::DOMElements fieldElements = Util::evalXPath (m_document, messagesPath);

		for (Util::DOMElements::iterator it = fieldElements.begin(); it != fieldElements.end(); ++it) 
		{	
			shared_ptr<NODE> nodeFieldPtr(new NODE());
			nodeFieldPtr->Name = "field";
			ATTRIBUTE attrName("name", Util::getAttribute ((*it), L"name") );
			ATTRIBUTE attrNumber("number", Util::getAttribute ((*it), L"number") );
			ATTRIBUTE attrType("type", Util::getAttribute ((*it), L"type") );

			nodeFieldPtr->m_AttributesPtr->push_back(attrName);
			nodeFieldPtr->m_AttributesPtr->push_back(attrNumber);
			nodeFieldPtr->m_AttributesPtr->push_back(attrType);

			// pick up value children elements !

			m_DictFields.insert( pair<string,shared_ptr<NODE> >(attrName.Value , nodeFieldPtr) );
		}
	}

	int FIXDictionary::GetFieldNumberByName(string fieldName)
	{
		for ( map<string, shared_ptr<NODE> >::iterator itDict = m_DictFields.begin() ; itDict != m_DictFields.end() ; itDict++ )
		{
			shared_ptr<NODE> nodeField = itDict->second;

			if ( !nodeField->GetAttribute("name").compare(fieldName) )
			{				
				return atoi( nodeField->GetAttribute("number").c_str() );
			}
		}

		return 0;
	}

	bool FIXDictionary::IsHeaderField(string fieldKey)
    {
		for ( CHILDNODES_SET::iterator itNodes = m_NodeHeaderPtr->m_ChildNodesPtr->begin(); itNodes != m_NodeHeaderPtr->m_ChildNodesPtr->end(); itNodes++)
		{			
			if ( !(*itNodes)->GetAttribute("name").compare(fieldKey) )
				return true;
		}

		return false;
    }

    bool FIXDictionary::IsTrailerField(string fieldKey)
    {
		for ( CHILDNODES_SET::iterator itNodes = m_NodeTrailerPtr->m_ChildNodesPtr->begin(); itNodes != m_NodeTrailerPtr->m_ChildNodesPtr->end(); itNodes++)
		{			
			if ( !(*itNodes)->GetAttribute("name").compare(fieldKey) )
				return true;
		}

		return false;
    }
	

	void FIXDictionary::LoadComponents(wstring messagesPath)
	{
		Util::DOMElements componentElements = Util::evalXPath (m_document, messagesPath);

		for (Util::DOMElements::iterator it = componentElements.begin(); it != componentElements.end(); ++it) 
		{	
			shared_ptr<NODE> NodeComponentPtr(new NODE());
			NodeComponentPtr->Name = "component";
			ATTRIBUTE attrName("name", Util::getAttribute ((*it), L"name") );
			NodeComponentPtr->m_AttributesPtr->push_back(attrName);

			Util::DOMElements compInnerElements = Util::childrenElements((*it));

			for (Util::DOMElements::iterator itComp = compInnerElements.begin(); itComp != compInnerElements.end(); ++itComp) 
			{
				if ( Util::verifyTypeOf( (Util::DOMElementPtr)(*itComp) , "field" ) )
				{
					LoadField(itComp, NodeComponentPtr);
				}

				if ( Util::verifyTypeOf( (Util::DOMElementPtr)(*itComp) , "component" ) )
				{	
					LoadComponent(itComp, NodeComponentPtr);
				}

				if ( Util::verifyTypeOf( (Util::DOMElementPtr)(*itComp) , "group" ) )
				{
					LoadGroup(itComp, NodeComponentPtr);
				}
			}

			m_DictComponents.insert( pair<string,shared_ptr<NODE> >(attrName.Value , NodeComponentPtr) );
		}
	}

	void FIXDictionary::LoadMessages(wstring messagesPath)
	{
		Util::DOMElements messageElements = Util::evalXPath (m_document, messagesPath);		

		for (Util::DOMElements::iterator it = messageElements.begin(); it != messageElements.end(); ++it) 
		{
			shared_ptr<NODE> NodeMessagePtr(new NODE());
			NodeMessagePtr->Name = "message";
			ATTRIBUTE attrName("name", Util::getAttribute ((*it), L"name") );
			ATTRIBUTE attrMsgType("msgtype", Util::getAttribute ((*it), L"msgtype") );

			NodeMessagePtr->m_AttributesPtr->push_back(attrName);
			NodeMessagePtr->m_AttributesPtr->push_back(attrMsgType);

			Util::DOMElements messageInnerElements = Util::childrenElements((*it));

			for (Util::DOMElements::iterator itMsg = messageInnerElements.begin(); itMsg != messageInnerElements.end(); ++itMsg) 
			{
				if ( Util::verifyTypeOf( (Util::DOMElementPtr)(*itMsg) , "field" ) )
				{
					LoadField(itMsg, NodeMessagePtr);
				}

				if ( Util::verifyTypeOf( (Util::DOMElementPtr)(*itMsg) , "component" ) )
				{	
					LoadComponent(itMsg, NodeMessagePtr);
				}

				if ( Util::verifyTypeOf( (Util::DOMElementPtr)(*itMsg) , "group" ) )
				{
					LoadGroup(itMsg, NodeMessagePtr);
				}
			}			

			m_DictMessages.insert( pair<string,shared_ptr<NODE> >(attrMsgType.Value , NodeMessagePtr) );
		}
	}
		
	void FIXDictionary::LoadGroup(Util::DOMElements::iterator itGroup , shared_ptr<NODE> & node)
	{
		shared_ptr<NODE> lNodeGroupPtr(new NODE());
		lNodeGroupPtr->Name = "group";
		ATTRIBUTE attr("name", Util::getAttribute ((*itGroup), L"name") );			
		lNodeGroupPtr->m_AttributesPtr->push_back(attr);

		Util::DOMElements messageGroupsElements = Util::childrenElements((*itGroup));

		for ( Util::DOMElements::iterator itInnerGroup = messageGroupsElements.begin(); itInnerGroup != messageGroupsElements.end(); itInnerGroup++ )
		{	
			if ( Util::verifyTypeOf( (Util::DOMElementPtr)(*itInnerGroup) , "field" ) )
			{
				LoadField( itInnerGroup, lNodeGroupPtr);
			}
			
			if ( Util::verifyTypeOf( (Util::DOMElementPtr)(*itInnerGroup) , "component" ) )
			{
				LoadComponent( itInnerGroup, lNodeGroupPtr);
			}
			
			if ( Util::verifyTypeOf( (Util::DOMElementPtr)(*itInnerGroup) , "group" ) )
			{
				LoadGroup( itInnerGroup, lNodeGroupPtr );
			}
		}

		node->m_ChildNodesPtr->push_back( lNodeGroupPtr );
		
	}

	void FIXDictionary::LoadComponent(Util::DOMElements::iterator itComponentElement , shared_ptr<NODE> & node)
	{
		shared_ptr<NODE> lNodeComponentPtr(new NODE());
		lNodeComponentPtr->Name = "component";
		ATTRIBUTE attr("name", Util::getAttribute ((*itComponentElement), L"name") );			
		lNodeComponentPtr->m_AttributesPtr->push_back(attr);

		Util::DOMElements componentElements = Util::childrenElements((*itComponentElement));

		for ( Util::DOMElements::iterator itInnerComp = componentElements.begin() ; itInnerComp != componentElements.end(); itInnerComp++)
		{	
			if ( Util::verifyTypeOf( (Util::DOMElementPtr)(*itInnerComp) , "field" ) )
			{
				LoadField(itInnerComp , lNodeComponentPtr);
			}
			
			if ( Util::verifyTypeOf( (Util::DOMElementPtr)(*itInnerComp) , "group" ) )
			{
				LoadGroup(itInnerComp, lNodeComponentPtr);
			}			
		}

		node->m_ChildNodesPtr->push_back(lNodeComponentPtr);
	}

	void FIXDictionary::LoadField(Util::DOMElements::iterator iter , shared_ptr<NODE> & node)
	{
		shared_ptr<NODE> lNodePtr(new NODE());
		lNodePtr->Name = "field";
		ATTRIBUTE attr("name", Util::getAttribute ((*iter), L"name") );			
		lNodePtr->m_AttributesPtr->push_back(attr);

		node->m_ChildNodesPtr->push_back(lNodePtr);
	}
	
	string FIXDictionary::GetMajorVersion()
	{
		return m_MajorVersion;		
	}

	string FIXDictionary::GetMinorVersion()
	{
		return m_MinorVersion;
	}

	void FIXDictionary::SetMessageOrderings(shared_ptr<MessageComponent> & p_messageComponent)
	{

		if (p_messageComponent->m_Type == TYPE_HEADER )
		{				
			SetMessageOrderings(p_messageComponent, m_NodeHeaderPtr);
		}
		else if (p_messageComponent->m_Type == TYPE_TRAILER )
		{	
			SetMessageOrderings(p_messageComponent, m_NodeTrailerPtr);            
		}
		else if (p_messageComponent->m_Type == TYPE_MESSAGE)
		{
			for ( map<string, shared_ptr<NODE> >::iterator itMessage = m_DictMessages.begin(); itMessage != m_DictMessages.end(); itMessage++ )
			{
				shared_ptr<NODE> msgNodePtr = itMessage->second;
				string dictName = msgNodePtr->GetAttribute("name");

				if ( !dictName.compare(p_messageComponent->m_Name) )
				{
					SetMessageOrderings(p_messageComponent, msgNodePtr );
					break;
				}
			}
		}
	}

	void FIXDictionary::SetMessageOrderings(shared_ptr<MessageComponent> & p_messageComponent, const shared_ptr<NODE> & node)
	{
		list<shared_ptr<MessageComponent> > componentOrdering;
		map<string, shared_ptr<MessageComponent> > remainingComponents = p_messageComponent->GetKeyedChildren();

		for ( CHILDNODES_SET::iterator itNode = node->m_ChildNodesPtr->begin(); itNode != node->m_ChildNodesPtr->end(); itNode++ )
		{
			if ( p_messageComponent->HasChild( (*itNode)->GetAttribute("name") ) )
			{
				shared_ptr<MessageComponent> innerMessageComponent = p_messageComponent->GetChild( (*itNode)->GetAttribute("name") );
				componentOrdering.push_back( innerMessageComponent );
				remainingComponents.erase( (*itNode)->GetAttribute("name") );

				if (innerMessageComponent->HasChildren())
                {
					if ( !(*itNode)->Name.compare("component") )
					{
						map<string, shared_ptr<NODE> >::iterator itComponent = m_DictComponents.find(innerMessageComponent->m_Name);
						shared_ptr<NODE> msgNodePtr = itComponent->second;
						SetMessageOrderings( innerMessageComponent , msgNodePtr );						
					} else if ( !(*itNode)->Name.compare("group") ) {
						list<shared_ptr<MessageComponent> > groupOrdering;

						list<shared_ptr<MessageComponent> > groupChildren = innerMessageComponent->GetChildren();
						for ( list<shared_ptr<MessageComponent> >::iterator itGroupChildren = groupChildren.begin(); itGroupChildren != groupChildren.end(); itGroupChildren++ )
						{
							const shared_ptr<NODE> groupNodePtr = (*itNode);
							SetMessageOrderings( *itGroupChildren , groupNodePtr );
							groupOrdering.push_back( *itGroupChildren );
						}
						
						innerMessageComponent->m_fieldOrdering.assign( groupOrdering.begin() , groupOrdering.end() );
					}
				}
			}			
		}		
		
		for ( map<string, shared_ptr<MessageComponent> >::iterator itRemaining = remainingComponents.begin(); itRemaining != remainingComponents.end() ; itRemaining++ )
		{
			componentOrdering.push_back(itRemaining->second);
		}
		
		p_messageComponent->m_fieldOrdering.assign( componentOrdering.begin() , componentOrdering.end() );
		
	}
	
};