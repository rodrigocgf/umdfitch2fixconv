#pragma once
#include "Config.h"

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include <boost/shared_ptr.hpp>
#include "MessageComponent.h"
#include "Util.h"
#include "StrX.h"


namespace UMDFITCH 
{
	using std::string;
	using std::map;
	using std::list;
	using std::wstring;
	using boost::shared_ptr;
	using boost::multi_index_container;
	using namespace boost::multi_index;
	XERCES_CPP_NAMESPACE_USE;	


	class FIXDictionary
	{	
	private:
		Config m_Config;

		XERCES_CPP_NAMESPACE::DOMDocument * m_document;
		
		void LoadFIXMessages();		
		
		void LoadVersion();
		void LoadHeader();
		void LoadTrailer();
		void LoadFields (wstring messagesPath);
		void LoadMessages(wstring messagesPath);
		void LoadComponents(wstring messagesPath);		
		
		void LoadField(Util::DOMElements::iterator iter , shared_ptr<NODE> & node);		
		void LoadComponent(Util::DOMElements::iterator itComponentElement , shared_ptr<NODE> & node);		
		void LoadGroup(Util::DOMElements::iterator itGroup , shared_ptr<NODE> & node);

		string m_MajorVersion;
		string m_MinorVersion;

		map<string, bool> m_headerFields;
        map<string, bool> m_trailerFields;
	public:
		FIXDictionary(void);
		FIXDictionary(Config lConfig);
		~FIXDictionary(void);

		/// <summary>
		/// FIX50SP2.UMDF.xml
		/// </summary>
		
		
		shared_ptr<NODE> m_NodeHeaderPtr;        
		shared_ptr<NODE> m_NodeTrailerPtr;	
		

		map<string, shared_ptr<NODE> > m_DictFields;
		map<string, shared_ptr<NODE> > m_DictMessages;
		map<string, shared_ptr<NODE> > m_DictComponents;

		int			GetFieldNumberByName(string fieldName);
		bool		IsHeaderField(string fieldKey);
		bool		IsTrailerField(string fieldKey);
		string		GetMajorVersion();
		string		GetMinorVersion();
		void		SetMessageOrderings(shared_ptr<MessageComponent> & p_messageComponent);
		void		SetMessageOrderings(shared_ptr<MessageComponent> & p_messageComponent, const shared_ptr<NODE> & node);		
	};

};