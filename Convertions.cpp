#include "StdAfx.h"
#include "Convertions.h"
#include "MessageComponent.h"

namespace UMDFITCH 
{
	Convertions::Convertions(void)
	{
	}

	Convertions::~Convertions(void)
	{
	}

	Convertions::Convertions(Config lConfig)
	{
		m_Config = lConfig;
		LoadITCH2FIXConvertions();
	}

	/// <summary>
	/// Load all convertions structures from ITCH2FIX.XML or ITCHAgg2FIX.XML
	/// </summary>
	void Convertions::LoadITCH2FIXConvertions()
	{
		Util::parseXML ( m_Config.m_ItchToFixConvertionFile , false, m_docITCH2FIX);

		LoadMappings();
	}

	void Convertions::LoadMappings()
	{		
		Util::DOMElements mappingsElements = Util::evalXPath (m_docITCH2FIX, L"/itch2fix/mappings/mapping");

		for (Util::DOMElements::iterator it = mappingsElements.begin(); it != mappingsElements.end(); ++it) 
		{
			MAPPING lMapping;

			lMapping.inMessageType =  Util::getAttribute ((*it),L"typeIn");
			lMapping.outMessageType = Util::getAttribute((*it),L"typeOut");
						
			
			Util::DOMElements convertionElements = Util::childrenElements((*it), "convertion");
			for ( Util::DOMElements::iterator itConvertions = convertionElements.begin(); itConvertions != convertionElements.end() ; itConvertions++ )
			{
				CONVERTION convertion;

				convertion.convertionType = Util::getAttribute((*itConvertions),L"type");

				
				if (	!convertion.convertionType.compare("copy") ||
						!convertion.convertionType.compare("ItchToFixPrice") ||
						!convertion.convertionType.compare("SaveAtVariable") ||
						!convertion.convertionType.compare("timeTransformITCH")
				)
				{
					Util::DOMElements sourceElement = Util::childrenElements((*itConvertions), "source");
					Util::DOMElements::iterator itSource = sourceElement.begin();
					
					convertion.m_Copy.sourceName = Util::getAttribute((*itSource),L"name");

					Util::DOMElements targetElement = Util::childrenElements((*itConvertions), "target");
					Util::DOMElements::iterator itTarget = targetElement.begin();

					convertion.m_Copy.targetName = Util::getAttribute((*itTarget),L"name");

				} 
				else if ( !convertion.convertionType.compare("fill") )
				{
					Util::DOMElements sourceElement = Util::childrenElements((*itConvertions), "filling");
					convertion.m_Copy.sourceFiller = Util::getAttribute(sourceElement[0],L"value");

					Util::DOMElements targetElement = Util::childrenElements((*itConvertions), "target");					
					convertion.m_Copy.targetName = Util::getAttribute(targetElement[0],L"name");
				}
				else if ( !convertion.convertionType.compare("switch") )
				{					
					convertion.m_Switch.m_origin = Util::getAttribute((*itConvertions),L"origin");
					convertion.m_Switch.m_name = Util::getAttribute((*itConvertions),L"name");

					Util::DOMElements caseElements = Util::childrenElements((*itConvertions), "case");

					for ( Util::DOMElements::iterator caseIterator = caseElements.begin(); caseIterator != caseElements.end(); caseIterator++ )
					{
						CASE innerCase;						
						innerCase.m_value = Util::getAttribute((*caseIterator),L"value");
						Util::DOMElements convElements = Util::childrenElements((*caseIterator));						
						shared_ptr<struct Convertion> innerConvertionPtr(new CONVERTION());
						innerConvertionPtr->convertionType = Util::getAttribute(convElements[0],L"type");

						if ( !innerConvertionPtr->convertionType.compare("copy")) 
						{
							Util::DOMElements sourceElements = Util::childrenElements(convElements[0],"source");
							innerConvertionPtr->m_Copy.sourceName = Util::getAttribute(sourceElements[0],L"name");

							Util::DOMElements targetElements = Util::childrenElements(convElements[0],"target");
							innerConvertionPtr->m_Copy.targetName = Util::getAttribute(targetElements[0],L"name");
						} 
						else if ( !innerConvertionPtr->convertionType.compare("fill")) 
						{
							Util::DOMElements sourceElement = Util::childrenElements(convElements[0], "filling");
							innerConvertionPtr->m_Copy.sourceFiller = Util::getAttribute(sourceElement[0],L"value");

							Util::DOMElements targetElement = Util::childrenElements(convElements[0], "target");					
							innerConvertionPtr->m_Copy.targetName = Util::getAttribute(targetElement[0],L"name");
						}

						innerCase.m_ConvertionPtr = innerConvertionPtr;
						convertion.m_Switch.m_listCases.push_back(innerCase);
					}
				}

				lMapping.m_listConvertions.push_back(convertion);
			}
			m_listMappings.push_back(lMapping);
		}
	}

	void Convertions::ExecuteDirectConvertion(shared_ptr<ITCHCracker> crackerPtr , shared_ptr<FIXAssembler> assemblerPtr)
	{
		ListMappings::nth_index<1>::type &inTypeIndex = m_listMappings.get<1>();
		
		ListMappings::nth_index<1>::type::iterator itMapping = inTypeIndex.find(crackerPtr->m_MessageType);

		if ( itMapping != inTypeIndex.end() )
		{
			MAPPING lMapping = *itMapping;	

			assemblerPtr->m_listMessageComponent.erase( assemblerPtr->m_listMessageComponent.begin() , assemblerPtr->m_listMessageComponent.end() );			
			assemblerPtr->SetMessageType(lMapping.outMessageType);

			for (	MAPPING::ListConvertions::iterator itConvertions = lMapping.m_listConvertions.begin(); 
					itConvertions != lMapping.m_listConvertions.end(); 
					itConvertions++ )
			{	
				MessageComponent messageComponent;
				string type = (*itConvertions).convertionType;

				if ( !type.compare("copy") )
				{
					string sourceName = (*itConvertions).m_Copy.sourceName;

					ITCHCracker::DictNameToMessageComponent::iterator itMsgType = crackerPtr->m_dictMessageComponent.find(sourceName);

					if ( itMsgType != crackerPtr->m_dictMessageComponent.end() )
					{
						FIELD fieldSource = itMsgType->m_Value;
						string targetName = (*itConvertions).m_Copy.targetName;
						messageComponent.m_Name = targetName;
						string sourceValue = fieldSource.Value;
						
						if (!sourceValue.empty())
						{
							std::string::size_type pos = sourceValue.find_first_not_of(' ');
							if (pos != std::string::npos)
								sourceValue.erase(0,pos);							

							std::string::size_type pos1 = sourceValue.find_last_not_of(' ');
							if (pos1 != std::string::npos)
								sourceValue.erase(pos1 + 1);							
						}

						FIELD fieldTarget(targetName, sourceValue , fieldSource.Type);
						
						messageComponent.m_Type = TYPE_MESSAGE;						
						messageComponent.m_Value = fieldTarget;						
						assemblerPtr->SetField(fieldTarget);						
					}					
				} 
				else if ( !type.compare("ItchToFixPrice")  )
				{
					string sourceName = (*itConvertions).m_Copy.sourceName;
					
					ITCHCracker::DictNameToMessageComponent::iterator itMsgType = crackerPtr->m_dictMessageComponent.find(sourceName);

					if ( itMsgType != crackerPtr->m_dictMessageComponent.end() )
					{
						FIELD fieldSource = itMsgType->m_Value;
						string targetName = (*itConvertions).m_Copy.targetName;						
						messageComponent.m_Name = targetName;

						string valuePrice;
						
						if ( fieldSource.Value.length() < 5 )
						{
							valuePrice.append( 5 - fieldSource.Value.length(),'0');
						}
						valuePrice.append( fieldSource.Value );

						string itchPrice = fieldSource.Value.substr(0,(valuePrice.length() - 4));
						itchPrice.append(".");
						itchPrice.append(valuePrice.substr((valuePrice.length() - 4),4));

						FIELD fieldTarget(targetName,itchPrice, fieldSource.Type);						
						messageComponent.m_Type = TYPE_MESSAGE;						
						messageComponent.m_Value = fieldTarget;
						
						assemblerPtr->SetField(fieldTarget);						
					}
				}
				else if ( !type.compare("SaveAtVariable") )
				{
					string sourceName = (*itConvertions).m_Copy.sourceName;
					string targetName = (*itConvertions).m_Copy.targetName;

					ITCHCracker::DictNameToMessageComponent::iterator itMsgType = crackerPtr->m_dictMessageComponent.find(sourceName);

					if ( itMsgType != crackerPtr->m_dictMessageComponent.end() )
					{
						FIELD fieldSource = itMsgType->m_Value;						
						string varName = targetName.substr( targetName.find_first_of('(') + 1, targetName.find_first_of(')') - targetName.find_first_of('(') - 1);

						map<string,string>::iterator itDictVar = m_dictVariables.find(varName);
						
						if ( itDictVar != m_dictVariables.end() )
						{
							m_dictVariables[varName] = fieldSource.Value;
						} else {
							m_dictVariables.insert( pair<string,string>(varName, fieldSource.Value) );
						}
					} 
				} 
				else if ( !type.compare("timeTransformITCH") )
				{
					string secondsSinceMidNight = m_dictVariables["SecondsSinceMidNight"];
					string sourceName = (*itConvertions).m_Copy.sourceName;
					string targetName = (*itConvertions).m_Copy.targetName;
					
					ITCHCracker::DictNameToMessageComponent::iterator itMsgType = crackerPtr->m_dictMessageComponent.find(sourceName);

					if ( itMsgType != crackerPtr->m_dictMessageComponent.end() )
					{
						FIELD fieldSource = itMsgType->m_Value;
						string strUTC = Util::itchTimeToUTCSystemTime( secondsSinceMidNight, fieldSource.Value );

						messageComponent.m_Name = targetName;
						FIELD fieldTarget(targetName , strUTC );
						messageComponent.m_Type = TYPE_MESSAGE;
						messageComponent.m_Value = fieldTarget;
						assemblerPtr->SetField(fieldTarget);

					}					
				}
				else if ( !type.compare("fill") )
				{
					string targetName = (*itConvertions).m_Copy.targetName;
					string sourceFiller = (*itConvertions).m_Copy.sourceFiller;
					
					messageComponent.m_Name = targetName;
					FIELD fieldTarget(targetName,sourceFiller);					
					messageComponent.m_Type = TYPE_MESSAGE;					
					messageComponent.m_Value = fieldTarget;
					
					assemblerPtr->SetField(fieldTarget);						
					
				}
				else if ( !type.compare("switch") )
				{
					string lOrigin = (*itConvertions).m_Switch.m_origin;

					if ( !lOrigin.compare("source") )
					{
						string sourceName = (*itConvertions).m_Switch.m_name;
					
						ITCHCracker::DictNameToMessageComponent::iterator itMsgType = crackerPtr->m_dictMessageComponent.find(sourceName);

						if ( itMsgType != crackerPtr->m_dictMessageComponent.end() )
						{
							FIELD fieldSource = itMsgType->m_Value;
							
							CASE_SET lListCases = (*itConvertions).m_Switch.m_listCases;							
							CASE_SET::nth_index<1>::type  &valueIndex = lListCases.get<1>();
							CASE_SET::nth_index<1>::type::iterator itCase = valueIndex.find( fieldSource.Value );

							if ( itCase != valueIndex.end() )
							{
								CASE lCase = *itCase;

								shared_ptr<CONVERTION> lConvertionPtr = lCase.m_ConvertionPtr;
								
								if ( !lConvertionPtr->convertionType.compare("copy") )
								{	
									string sourceNameInner = lConvertionPtr->m_Copy.sourceName;
					
									ITCHCracker::DictNameToMessageComponent::iterator itMsgTypeInner = crackerPtr->m_dictMessageComponent.find(sourceNameInner);

									if ( itMsgTypeInner != crackerPtr->m_dictMessageComponent.end() )
									{
										FIELD fieldSourceIn = itMsgTypeInner->m_Value;
										string targetNameIn = lConvertionPtr->m_Copy.targetName;
										
										messageComponent.m_Name = targetNameIn;
										FIELD fieldTargetIn ( targetNameIn , fieldSourceIn.Value, fieldSourceIn.Type);										
										messageComponent.m_Type = TYPE_MESSAGE;										
										messageComponent.m_Value = fieldTargetIn;
										
										assemblerPtr->SetField(fieldTargetIn);						
									}

								} 
								else if ( !lConvertionPtr->convertionType.compare("fill") )
								{
									string targetNameIn = lConvertionPtr->m_Copy.targetName;
									string sourceFillerIn = lConvertionPtr->m_Copy.sourceFiller;
									
									messageComponent.m_Name = targetNameIn;
									FIELD fieldTargetIn( targetNameIn , sourceFillerIn );
									messageComponent.m_Type = TYPE_MESSAGE;					
									messageComponent.m_Value = fieldTargetIn;
									
									assemblerPtr->SetField(fieldTargetIn);						
								}
							}

						}
					}
				}
			}

		}
	
	}


};