#pragma once

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "Config.h"
#pragma warning (push)
#pragma warning (disable : 4146 4267)
#include "FIXDictionary.h"
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>
#include <quickfix/Values.h>
#include <quickfix/Fields.h>
#include <quickfix/Message.h>
#pragma warning (pop)
//#include "MessagePathEnumerator.h"
#include "MessageComponent.h"
#include "MessagePath.h"
#include "Util.h"

namespace UMDFITCH 
{
	using std::string;
	using std::list;	
	using boost::shared_ptr;
	using boost::multi_index_container;
	using namespace boost::multi_index;	

	class FIXAssembler
	{
	public:
		string					m_MessageType;

		FIXAssembler(void);
		FIXAssembler(Config lConfig);
		~FIXAssembler(void);

		
		/// <summary>
		///	List MESSAGECOMPONENT
		/// </summary>
		list<MessageComponent> m_listMessageComponent;		
		Config m_Config;

		/// <summary>
		/// FIX Messages from FIX50SP2.UMDF.xml
		/// </summary>
		shared_ptr<FIXDictionary> FIXDictionaryPtr;

		shared_ptr<FIX::Message> Assemble();
		void				SetMessageType(string msgType);
		void				SetField(FIELD field);
		int					CalculateFieldLength(FIELD field);
		string				FieldToString(FIELD field);
		void				GetMessagePayload(shared_ptr<MessageComponent> messageElement, string & message);
		string				CompileToString();
		void				BuildHeaderAndTrailer();
		void				FillBodyLength(string &message);
		void				CalculateChecksum(string &message);

		class Checksummer
		{
		private:
			unsigned char m_checksum;
		public:

			Checksummer()
			{
				m_checksum = 0;
			}

			void AddFieldToChecksum(FIELD field)
			{
				for ( std::string::iterator itChar = field.Name.begin(); itChar != field.Name.end() ; itChar++ )
				{
					m_checksum += (unsigned char)*itChar;
				}
				m_checksum += (unsigned char)(SEPARATOR);

				for ( std::string::iterator itChar1 = field.Value.begin(); itChar1 != field.Value.end() ; itChar1++ )
				{
					m_checksum += (unsigned char)*itChar1;
				}
				m_checksum += (unsigned char)(TERMINATOR);			

			}

			void RemoveFieldFromChecksum(FIELD field)
			{
				for ( std::string::iterator itChar = field.Name.begin(); itChar != field.Name.end() ; itChar++ )
				{
					m_checksum -= (unsigned char)*itChar;
				}
				m_checksum -= (unsigned char)(SEPARATOR);

				for ( std::string::iterator itChar1 = field.Value.begin(); itChar1 != field.Value.end() ; itChar1++ )
				{
					m_checksum -= (unsigned char)*itChar1;
				}
				m_checksum -= (unsigned char)(TERMINATOR);				
			}


			string GetChecksum()
			{				
				char szChecksum[4];
				memset(szChecksum,0x00,sizeof(szChecksum));
				sprintf_s(szChecksum,"%03d",(unsigned int)(m_checksum%256));
				string strChecksum = szChecksum;

				return strChecksum;
			}
		};

	private:
		//Checksummer m_checksum;

		shared_ptr<MessageComponent> m_headerPtr;
        shared_ptr<MessageComponent> m_bodyPtr;
        shared_ptr<MessageComponent> m_trailerPtr;
		FIX::DataDictionary			 m_fIX50DataDictionary;

		int m_iBodyLength;
		double m_dMsgSeqNum;
		void SetField(FIELD field, shared_ptr<MessageComponent> & p_messageComponent, shared_ptr<MessagePathEnumerator> & p_fieldPathEnumerator);

	};

};