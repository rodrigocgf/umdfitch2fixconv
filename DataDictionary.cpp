#include "StdAfx.h"
#include "DataDictionary.h"


namespace UMDFITCH 
{
	

	DataDictionary::DataDictionary(Config lConfig)
	{
		Util::parseXML ( lConfig.m_fIX50DataDictionary , false, m_document);

		m_protocolName = "fix";//_dataDictionary.FirstChild.Name;

		MountHeader();
        MountTrailer();
        MountFieldsDictionary();
        MountMessagesDictionary();
        MountComponentsDictionary();
	}

	DataDictionary::~DataDictionary(void)
	{
	}

	DataDictionary::MountHeader()
	{
		//_xmlNodeHeader = _dataDictionary.SelectSingleNode("/" + _protocolName + "/header");            
	}

    DataDictionary::MountTrailer()
	{

	}

    DataDictionary::MountFieldsDictionary()
	{

	}

    DataDictionary::MountMessagesDictionary()
	{

	}

    DataDictionary::MountComponentsDictionary()
	{

	}


}