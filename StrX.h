#pragma once

#include <tchar.h>
#include <stdexcept>
#include <xercesc/dom/DOMErrorHandler.hpp>
#include <xercesc/util/XMLString.hpp>
#include <boost/shared_ptr.hpp>

/// <summary>
/// </summary>
class StrX
{
public:
    /// <summary>Constructor
    /// <summary>
    StrX (const XMLCh* toTranscode) : pLocalForm (0)
    {
        if (toTranscode)
        {
            pLocalForm = XERCES_CPP_NAMESPACE::XMLString::transcode(toTranscode);
        }
    }
    /// <summary>Destructor
    /// <summary>
    ~StrX ()
    {
        if (pLocalForm) 
        {
            XERCES_CPP_NAMESPACE::XMLString::release(&pLocalForm);
        }
    }

    /// <summary>getter
    /// <summary>
    const char* localForm() const 
    {
        return pLocalForm;
    }

    /// <summary>getter
    /// <summary>
    const std::string toString() const 
    {
        return (pLocalForm) ? pLocalForm : "";
    }

    __int32 toInt32 () const
    {
        return pLocalForm ? (__int32)strtol (pLocalForm, NULL, 10) : 0;
    }

    /// <summary> 
	/// true if the string is not NULL, false otherwise
    /// </summary>
public:
    bool operator ! ()
    {
        return pLocalForm != 0;
    }

public:
    /// <summary>Utility method - see toString
    /// <summary>
    static void assign (std::string& str, const XMLCh* const toTranscode) 
    {
        if (toTranscode != NULL)
        {
		    char* p = XERCES_CPP_NAMESPACE::XMLString::transcode(toTranscode);
            str.assign (p);
            XERCES_CPP_NAMESPACE::XMLString::release(&p);
        }
        else
            str.clear();
    }
    /// <summary>Utility method - see toString
    /// <summary>
    static void assign (int& val, const XMLCh* const toTranscode) 
    {
        if (toTranscode != NULL)
        {
            val = _wtoi (toTranscode);
        }
        else
            val = 0;
    }
    static void assign (boost::shared_ptr<std::string>& shptr, const XMLCh* const toTranscode) 
    {
        if (toTranscode != NULL)
        {
		    char* p = XERCES_CPP_NAMESPACE::XMLString::transcode(toTranscode);
            shptr.reset (new std::string (p));
            XERCES_CPP_NAMESPACE::XMLString::release(&p);
        }
        else
            shptr.reset ();
    }
    static bool equals (const XMLCh* const toTranscode, const char* val)
    {
        bool result = false;
        if (toTranscode != NULL)
        {
		    char* p = XERCES_CPP_NAMESPACE::XMLString::transcode(toTranscode);
            result = (strcmp (p, val) == 0);
            XERCES_CPP_NAMESPACE::XMLString::release(&p);
        }
        return result;
    }
    static bool equals (const XMLCh* const toTranscode, const std::string& val)
    {
        bool result = false;
        if (toTranscode != NULL)
        {
		    char* p = XERCES_CPP_NAMESPACE::XMLString::transcode(toTranscode);
            result = val.compare (p) == 0;
            XERCES_CPP_NAMESPACE::XMLString::release(&p);
        }
        return result;
    }
    static std::string toString (const XMLCh* const toTranscode)
    {
        std::string result;
        if (toTranscode != NULL)
        {
		    char* p = XERCES_CPP_NAMESPACE::XMLString::transcode(toTranscode);
            result = p;
            XERCES_CPP_NAMESPACE::XMLString::release(&p);
        }
        return result;
    }
    static boost::shared_ptr<std::string> toString (const XMLCh* const toTranscode, bool)
    {
        boost::shared_ptr<std::string> result;
        if (toTranscode != NULL)
        {
		    char* p = XERCES_CPP_NAMESPACE::XMLString::transcode(toTranscode);
            result.reset (new std::string (p));
            XERCES_CPP_NAMESPACE::XMLString::release(&p);
        }
        return result;
    }
private:
    char* pLocalForm;
};

inline XERCES_STD_QUALIFIER ostream& operator<<(XERCES_STD_QUALIFIER ostream& target, const StrX& toDump)
{
    target << toDump.localForm();
    return target;
}
