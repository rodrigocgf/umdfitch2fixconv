#pragma once
#include <string>
#include <vector>
#include <math.h>
#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMElement.hpp>
#include <boost/version.hpp>
#pragma warning (push)
#pragma warning (disable : 4146 4267)
#include <quickfix/Message.h>
#pragma warning (pop)

#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/sequenced_index.hpp>
#include <boost/multi_index/hashed_index.hpp>
#include <boost/multi_index/identity.hpp>
#include <boost/multi_index/member.hpp>

namespace UMDFITCH
{ 
	#define TERMINATOR 0x01 
	#define SEPARATOR '='

	using std::string;
	using std::wstring;
	using std::vector;
	using std::list;
	using std::map;
	using boost::shared_ptr;
	using std::string;
	using std::wstring;
	using boost::multi_index_container;
	using namespace boost::multi_index;
	XERCES_CPP_NAMESPACE_USE;

	
	typedef struct Copy
	{
		string	sourceName;
		string	sourceFiller;
		string	targetName;
	} COPY;

	struct Convertion;

	typedef struct Case
	{		
		string	m_value;
		shared_ptr<struct Convertion> m_ConvertionPtr;		
	} CASE;

	typedef multi_index_container<
		CASE,
		indexed_by<
			sequenced< >,
			hashed_unique< member<CASE,std::string,&CASE::m_value> >
		>
	> CASE_SET;

	typedef struct Switch
	{			
		string m_origin;
		string m_name;		
		CASE_SET m_listCases;	
		
	} SWITCH;

	typedef struct Convertion
	{
		string	convertionType;		
				
		COPY	m_Copy;
		SWITCH	m_Switch;

	} CONVERTION;

	typedef struct Mapping
	{
		string	inMessageType;
		string	outMessageType;

		typedef multi_index_container<
			CONVERTION,
			indexed_by< 
				sequenced< >
			>
		> ListConvertions;
		ListConvertions		m_listConvertions;
	} MAPPING;

	
	typedef multi_index_container<
		MAPPING,
		indexed_by< 
			sequenced< >,				
			ordered_unique<member<MAPPING,std::string, &MAPPING::inMessageType> >
		>
	> ListMappings;
	
	class FIELD;
	class GROUP;
	class COMPONENT;

	
	typedef multi_index_container<
		shared_ptr<FIELD>,
		indexed_by< 
			sequenced<>,
			ordered_unique<identity<shared_ptr<FIELD> > >
		>			
	> Fields_set;
	
	typedef multi_index_container<
		shared_ptr<GROUP>,
		indexed_by< 
			sequenced<>
		>			
	> Groups_set;
	
	typedef multi_index_container<
		shared_ptr<COMPONENT>,
		indexed_by< 
			sequenced<> ,
			ordered_unique<identity<shared_ptr<COMPONENT> > >
		>			
	> Components_set;	
	
	
	class FIELD
	{
	public:
		FIELD(){}
		FIELD(string name, string value):
			Name(name),Value(value),Type("0"),Position(0),Length(0)
		{
		}

		FIELD(string name, string value, string type):
			Name(name),Value(value),Type(type),Position(0),Length(0)
		{
		}
		FIELD(string name, string value, string type, int position ):
			Name(name),Value(value),Type(type),Position(position),Length(0)
		{
		}
		FIELD(string name, string value, string type, int position , int length):
			Name(name),Value(value),Type(type),Position(position),Length(length)
		{
		}		

		FIELD& operator=(const FIELD & in);		

		string	Name;
		string  Value;
		int		Position;
		int		Length;
		string	Number;
		string	Type;
		string	Description;
	};

	
	
	class COMPONENT
	{
	public:
		string	Name;
		string	Type;
		int		Length;
		
		COMPONENT() :	m_FieldsPtr(new Fields_set()),
						m_GroupsPtr(new Groups_set())
		{

		}
		
		shared_ptr<Fields_set> m_FieldsPtr;
		shared_ptr<Groups_set> m_GroupsPtr;
	};

	class GROUP
	{
	public:
		string	Name;
		string	Type;
		int		Length;
		
		GROUP() :	m_GroupsPtr(new Groups_set()),
					m_FieldsPtr(new Fields_set()),
					m_ComponentsPtr( new Components_set())
		{

		}

		GROUP(string lName) :	m_GroupsPtr(new Groups_set()),
								m_FieldsPtr(new Fields_set()),
								m_ComponentsPtr( new Components_set()),
								Name(lName)
		{
			
		}
		
		shared_ptr<Groups_set> m_GroupsPtr;
		shared_ptr<Fields_set> m_FieldsPtr;		
		shared_ptr<Components_set> m_ComponentsPtr;
	};

	
	class MESSAGE
	{
	public:
		string	Name;
		string	Type;
		int		Length;
	
		MESSAGE() : m_FieldsPtr(new Fields_set()), 
					m_GroupPtr(new Groups_set()), 
					m_ComponentsPtr(new Components_set())
		{

		}
		
		shared_ptr<Fields_set> m_FieldsPtr;		
		shared_ptr<Groups_set> m_GroupPtr;
		shared_ptr<Components_set> m_ComponentsPtr;

	};
	
	typedef struct Attribute
	{
		Attribute(){}
		Attribute(string pKey, string pValue): Key(pKey),Value(pValue) {}

		string Key;
		string Value;
	} ATTRIBUTE;
	
	typedef multi_index_container<
		ATTRIBUTE,
		indexed_by<
			sequenced< >,				
			hashed_unique< member<ATTRIBUTE,string, &ATTRIBUTE::Key > >
		>
	> ATTRIBUTES_SET;

	class NODE;

	typedef multi_index_container<
		shared_ptr<NODE>,
		indexed_by<
			sequenced< >,
			hashed_unique< identity< shared_ptr<NODE> > >
		>
	> CHILDNODES_SET;

	class NODE
	{
	public:
		NODE() :	m_AttributesPtr( new ATTRIBUTES_SET() ), 
					m_ChildNodesPtr( new CHILDNODES_SET() )
		{

		}

		string						Name;		
		shared_ptr<ATTRIBUTES_SET>	m_AttributesPtr;		
		shared_ptr<CHILDNODES_SET>	m_ChildNodesPtr;	
		
		string GetAttribute(string name) 
		{
			ATTRIBUTES_SET::nth_index<1>::type  &keyIndex = m_AttributesPtr->get<1>();
			ATTRIBUTES_SET::nth_index<1>::type::iterator itKey = keyIndex.find( name );

			if ( itKey != keyIndex.end() )
				return itKey->Value;			

			return "";
		}
	};
	

	class Util
	{
	public:
		typedef const XERCES_CPP_NAMESPACE::DOMElement* DOMElementPtr;
		typedef vector<DOMElementPtr> DOMElements;
	public:		
		static bool verifyTypeOf(DOMElementPtr parentElement , const string& elementName);
		static bool hasElement(DOMElementPtr parentElement ,const wstring& elementName);
		static string getAttribute (DOMElementPtr element, const wstring& attributeName);
		static string getAttribute (DOMElementPtr element, const wstring& attributeName, const string& defaultValue);
		static DOMElementPtr childElement(const DOMElement * root );
		static DOMElements childrenElements (DOMElementPtr root);
		static DOMElements childrenElements (DOMElementPtr root, const string& nodeName);
		static DOMElements evalXPath (const XERCES_CPP_NAMESPACE::DOMDocument* document, const wstring& xpathExpression);
		static void parseXML(const string& filename, bool validate, XERCES_CPP_NAMESPACE::DOMDocument*& document);    
		static bool booleanValue (const string& str);
		static string format (const char* fmt, ...);
		static int parseInt (const string& str);
		static string normalize (const string& str);
		static string capitalize (const string& str);
		static string right (const string& str, size_t n);
		static string toFormattedString (const FIX::Message& msg, const FIX::DataDictionary &dd);
	public:
		/// <example> &lt;date&gt; source="Order Timestamp"	&lt;__expr__&gt; </example>
		static string toFIXDate (const string& str);
		/// <example> &lt;time&gt; source="Order Timestamp"	&lt;__expr__&gt; </example>
		static string toFIXTime (const string& str);
		/// <example> rLCTimeToFIXTime ("123456") ==> "153456" </example>
		static string rLCTimeToFIXTime (const string& str);
		/// <example> rLCSignToFIXTickDirection ("+") ==> FIX::TickDirection_PLUS_TICK</example>
		static char rLCSignToFIXTickDirection (const string& str);
		/// <example> &lt;time&gt; source="Order Timestamp"	&lt;__expr__&gt; </example>
		static void toFIXDateAndTime (const string& str, string& date, string& time);
		/// <example> &lt;time&gt; source="Order Timestamp"	&lt;__expr__&gt; </example>
		static void toMDEntryDateAndTime (const string& str, FIX::MDEntryDate& date, FIX::MDEntryTime& time);
		/// <example> &lt;time&gt; source="Order Timestamp"	&lt;__expr__&gt; </example>
		static string toFIXTimestamp (const string& str);
		/// <example> &lt;time&gt; source="Order Timestamp"	&lt;__expr__&gt; </example>
		static FIX::UtcTimeStamp toUtcTimestamp (const string& str);
		/// <example> rLCTimeToFIXTimestamp  ("123456") ==> "20100308-23:59:59" </example>
		static string rLCTimeToFIXTimestamp (const string& str);

		static string itchTimeToUTCSystemTime ( const string & secondsSinceMidNight, const string & nanoseconds );
		static void localStringTimestampToUTCSystemTime (const string& str, SYSTEMTIME& st);
		static void localTimeToUTCSystemTime (const string& str, SYSTEMTIME& st);
		static string lpad (const string& str, size_t length, const string& fill);
		static string rpad (const string& str, size_t length, const string& fill);
		static string zerofill (const string& str, size_t length);    
		static bool tooLate (const string& timestamp, int ms);
		static string filterNonAsciiChars (const string& str);
		static string join (const string& separator, const string& str1);
		static string join (const string& separator, const string& str1, const string& str2);
		static string join (const string& separator, const string& str1, const string& str2, const string& str3);
		static string join (const string& separator, const string& str1, const string& str2, const string& str3, const string& str4);
		static string join (const string& separator, const string& str1, const string& str2, const string& str3, const string& str4, const string& str5);
		/// <summary>If the termination event is signaled (or maybe invalid)</summary>
		/// <returns>true if the event is signaled, or invalid, or abandoned</returns>
		static bool isSignaledTerminationEvent (HANDLE h);
	    
		/// <summary>Finds the nth matching field value for a given tag in a FIX::Group serialized as a string.</summary>
		/// <note>A horrible, disgusting hack; needed for dealing with the SecurityList plugin, that does not use a FIX::Group.
		/// Can break if you have nested repeating groups. Binary fields also break this method.</note>
		/// <param name='tagNo'>The tag number (like 1151 for FIX::FIELD::SecurityGroupStd)</param>
		/// <param name='serializedGroup'>The FIX::Group, serialized as a string</param>
		/// <param name='occurrence'>1 = first occurrence, 2 = second occurrence etc.</param>
		/// <returns>An empty string if not found. The field value, if found.</returns>
		static string getMatchingFieldValue (int tagNo, const string& serializedGroup, int occurrence = 1);

		/// <summary>Finds the first matching field value for a collection of tags in a FIX::Group serialized as a string.</summary>
		/// <note>A horrible, disgusting hack; needed for dealing with the SecurityList plugin, that does not use a FIX::Group.
		/// Can break if you have nested repeating groups. Binary fields also break this method.</note>
		/// <param name='serializedGroup'>The FIX::Group, serialized as a string</param>
		/// <param name='firstTag'>The tag number (like 1151 for FIX::FIELD::SecurityGroupStd)</param>
		/// <param name='...'>A list of alternative tag numbers, ended by a 0 argument.</param>
		/// <returns>An empty string if not found. The field value, if found.</returns>
		static string getFirstMatchingFieldValue (const string& serializedGroup, int firstTag, ...);
	private:
		static string toFormattedString (const string& msgType, const FIX::FieldMap& fmap, const FIX::DataDictionary& dd, int level, const FIX::DataDictionary& globalDD);
		static bool xerces_initialized;
	};


 };

#include <boost/exception.hpp>
#if BOOST_VERSION > 103600
    #define BOOST_DIAGNOSTIC_INFORMATION(e) \
        boost::diagnostic_information(e)
#else
    #define BOOST_DIAGNOSTIC_INFORMATION(e) \
        (e).diagnostic_information()
#endif



