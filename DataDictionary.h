#pragma once

#include <xercesc/dom/DOMNode.hpp>
#include <xercesc/dom/DOMDocument.hpp>
#include <xercesc/dom/DOMElement.hpp>
//#include "StrX.h"

namespace UMDFITCH 
{

	class DataDictionary
	{
		using std::map;
		using std::string;
		XERCES_CPP_NAMESPACE_USE;	
	private:
		DOMDocument m_dataDictionary;
        string		m_protocolName;

        DOMNode		m_domNodeHeader;
        DOMNode		m_domNodeTrailer;
	public:		
		DataDictionary(Config lConfig);
		~DataDictionary(void);
		void MountHeader();
        void MountTrailer();
        void MountFieldsDictionary();
        void MountMessagesDictionary();
        void MountComponentsDictionary();

		map<string, DOMNode> FieldsDictionary;
		map<string, DOMNode> MessagesDictionary;
		map<string, DOMNode> ComponentsDictionary;
	};

}