#include "stdafx.h"
#pragma warning (push)
#pragma warning (disable : 4146 4267 4996)
#include "UMDFITCH2FIXConv.h"
#include "BmfWin32/ILog.h"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/foreach.hpp>
#include <quickfix/fix50sp2/fieldnumbers.h>
#pragma warning (pop)

namespace UMDFITCH 
{ 
	namespace Plugin 
	{
		
		DECLARE_FREEINSTANCE_PLUGIN_FACTORY(UMDFITCH2FIXConverter)
		
		UMDFITCH2FIXConverter::UMDFITCH2FIXConverter(LPCTSTR strPluginID) 						
		{
			m_pluginID = strPluginID;
			//m_securityProviderEntryPoint = NULL;
			m_shutdownEvent = NULL;			
		}
		
		UMDFITCH2FIXConverter::~UMDFITCH2FIXConverter()
		{
		}
		
		bool UMDFITCH2FIXConverter::OnLoad()
		{	
			if ( ! m_pGatewayApp)
			{
				GW_ERROR_FORMAT(_T("[%s(%s)] - Gateway application is NULL."), __FUNCTION__ % GetPluginID());
				return false;
			}
			
			vector<pair<string, int> > errors (m_config.loadConfiguration (m_pGatewayApp, this, m_pluginID));

			if ( ! errors.empty()) 
			{
				// Note que devemos checar se h� algum erro, ou se a indica��o � menos grave.
				int maxErrorLevel = BmfWin32::LTRACE;
				for (size_t i = 0; i < errors.size(); ++i) 
				{
					pair<string, int> error = errors[i];
					if (error.second <= maxErrorLevel) 
						maxErrorLevel = error.second;
				}
				if (maxErrorLevel == BmfWin32::LERROR) 
				{
					GW_ERROR_FORMAT(_T("[%s(%s)] - Errors found when reading the configuration file."), 
						__FUNCTION__ % GetPluginID());
				} 
				else 
				{
					GW_WARNING_FORMAT(_T("[%s(%s)] - Errors found when reading the configuration file."), 
						__FUNCTION__ % GetPluginID());
				}
				for (size_t i = 0; i < errors.size(); ++i) 
				{
					pair<string, int> error = errors[i];
					switch (error.second) 
					{
					case BmfWin32::LERROR:
						GW_ERROR_FORMAT(_T("[%s(%s)] - %s."),
							__FUNCTION__ % this->GetPluginID() % error.first)
						break;
					case BmfWin32::LWARNING:
						GW_WARNING_FORMAT(_T("[%s(%s)] - %s."),
							__FUNCTION__ % this->GetPluginID() % error.first)
						break;
					case BmfWin32::LINFO:
						GW_INFO_FORMAT(_T("[%s(%s)] - %s."),
							__FUNCTION__ % this->GetPluginID() % error.first)
						break;
					case BmfWin32::LDEBUG:
						GW_DEBUG_FORMAT(_T("[%s(%s)] - %s."),
							__FUNCTION__ % this->GetPluginID() % error.first)
						break;
					case BmfWin32::LTRACE:
						GW_TRACE_FORMAT(_T("[%s(%s)] - %s."),
							__FUNCTION__ % this->GetPluginID() % error.first)
						break;
					}
				}
			}

			m_shutdownEvent = CreateEvent ( NULL,TRUE, FALSE, NULL);

			// Loading the FIX 5.0 Data Dictionary
			try
			{
				m_fIX50DataDictionary.readFromURL(m_config.m_fIX50DataDictionary);
			}
			catch (FIX::ConfigError& configerror)
			{
				GW_ERROR_FORMAT(_T("%s(%s) - Error loading FIX5.0 data dictionary [%s]: %s."), 
					__FUNCTION__ % GetPluginID() % m_config.m_fIX50DataDictionary % configerror.what());

				return false;
			}

			// Getting the MD Broadcaster Plugin Entry Point
			for (int i = 0; i < m_config.m_iNumberOfBroadcasters; ++i) 
			{
				BTS::IMessageEntryPoint * pt = m_pGatewayApp->GetMessageEntryPoint (m_config.m_vecMDBroadcasterEntryNames[i].c_str());
				if (pt)
				{
					m_vecMDBroadcasterEntryPoint.push_back (pt);
        			GW_WARNING_FORMAT(	"%s(%s) - MDBroadcaster entry point=[%s] loaded successfully at {0x%X}!", 
										__FUNCTION__ % GetPluginID() % m_config.m_vecMDBroadcasterEntryNames[i] % pt);
				}
				else
				{
					GW_ERROR_FORMAT(	_T("%s(%s) - MDBroadcaster entry point=[%s] is not valid!"), 
										__FUNCTION__ % GetPluginID() % m_config.m_vecMDBroadcasterEntryNames[i]);
					return false;
				}
			}
						
			ITCHCrackerPtr.reset( new ITCHCracker(m_config) );			
			FIXAssemblerPtr.reset( new FIXAssembler(m_config) );			
			convertionsPtr.reset( new Convertions(m_config) );

			return true;
		}

		bool UMDFITCH2FIXConverter::OnStart()
		{
			receiverPtr = ReceiverFactory::CreateReceiver(m_config,this);
			receiverPtr->Start();

			return true;
		}
		
		void UMDFITCH2FIXConverter::OnStop()
		{
			//shutdownThreads();
		}
		
		void UMDFITCH2FIXConverter::OnLogon(const FIX::SessionID &)
		{
			
		}
		
		void UMDFITCH2FIXConverter::OnLogout(const FIX::SessionID &)
		{
			
		}
		
		LPCTSTR UMDFITCH2FIXConverter::GetPluginID() const
		{
			return m_pluginID.c_str();
		}
		
		UINT UMDFITCH2FIXConverter::OnMessage(const FIX::Message &,const BTS::Sender &)
		{
			// just pass it ahead
			return TRUE;
		}

		bool UMDFITCH2FIXConverter::OnSecurities(const vector<IUMDFSecuritySubscriber::SecurityData>& securities, const string& channel)
		{			
			return true;
		}

		/// <summary>
		///	Main convertion function
		/// </summary>

		void UMDFITCH2FIXConverter::ExecuteConvertion(unsigned char * buffer, int size)
		{
			string strTime;
			SYSTEMTIME localTime;

			GW_INFO_FORMAT(_T("%s")," ");
			GW_INFO_FORMAT(_T("%s")," VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV ");
			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- CRACK : %s"),strTime);
			ITCHCrackerPtr->Crack(buffer,size);

			
			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- CONVERTION : %s"),strTime);
			convertionsPtr->ExecuteDirectConvertion(ITCHCrackerPtr, FIXAssemblerPtr); 

			
			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- ASSEMBLE : %s"),strTime);
			shared_ptr<FIX::Message> fixMsgPtr =  FIXAssemblerPtr->Assemble();
			
			::GetLocalTime(&localTime);
			strTime = Util::format("%02d:%02d:%02d.%03d",localTime.wHour,localTime.wMinute,localTime.wSecond,localTime.wMilliseconds);
			GW_INFO_FORMAT(_T(" ---- BROADCAST : %s"),strTime);

			BOOST_FOREACH (BTS::IMessageEntryPoint* entryPoint, m_vecMDBroadcasterEntryPoint)
			{
				entryPoint->OnMessage(*fixMsgPtr, m_pluginID );
			}
			
			GW_INFO_FORMAT(_T("%s")," ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ");
			GW_INFO_FORMAT(_T("%s")," ");
		}


	}; 
};


#ifdef _MANAGED
#pragma managed(push, off)
#endif

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{	
    return TRUE;
}

#ifdef _MANAGED
#pragma managed(pop)
#endif

