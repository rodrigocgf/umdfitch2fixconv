#pragma once

#include "MessagePathElement.h"

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace UMDFITCH 
{
	using std::string;
	using std::vector;
	using boost::shared_ptr;

	class MessagePath : public boost::enable_shared_from_this<MessagePath>
	{
	friend class MessagePathEnumerator;
	public:		
		boost::shared_ptr<MessagePath> get_this_shared_ptr()
		{
			return shared_from_this();
		}

		MessagePath(string rawPath);
		~MessagePath(void);

		shared_ptr<MessagePathEnumerator> GetEnumerator();
		shared_ptr<MessagePathElement> GetLastPathElement();

	private:
		string m_rawPath;
		vector<shared_ptr<MessagePathElement> > m_pathElements;		
	};

	class MessagePathEnumerator 
	{
	public:		
		MessagePathEnumerator();
		~MessagePathEnumerator(void);

		void SetParent(MessagePath * p_messagePath);

		string GetUpdatedPathElement();
		shared_ptr<MessagePathEnumerator> Clone();
		void Reset();
		bool MoveNext();
		bool HasMoreElements();
		string GetCleanNamespace();
		shared_ptr<MessagePathElement> Current();
	private:
		
		MessagePath *		m_pMessagePath;
		string		m_pathAsNamespaceBuilder;
		int			m_index;
	};


};