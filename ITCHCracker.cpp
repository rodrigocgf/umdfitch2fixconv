#include "StdAfx.h"
#include "ITCHCracker.h"


namespace UMDFITCH 
{
	ITCHCracker::ITCHCracker(void)
	{

	}

	ITCHCracker::ITCHCracker(Config lConfig)
	{
		m_Config = lConfig;
		m_iCurrentPosition = 0;				

		// Load ITCH messages from ITCHMessages.xml		
		ITCHDictionaryPtr.reset( new ITCHDictionary(m_Config) );
	}

	ITCHCracker::~ITCHCracker(void)
	{
	}

	void ITCHCracker::Crack(unsigned char * buffer , int size)
	{	
		int    msgLength;		
		
		m_dictMessageComponent.erase(m_dictMessageComponent.begin() , m_dictMessageComponent.end());	

		string type(1, buffer[0]);				
		m_MessageType = type;

		ITCHDictionary::Messages_set::iterator it = ITCHDictionaryPtr->m_Messages.find(type);
		shared_ptr<Fields_set> msgFieldsPtr;		
		if ( it != ITCHDictionaryPtr->m_Messages.end() )
		{
			m_MessageName = (*it)->Name;
			msgLength = (*it)->Length;			
			msgFieldsPtr = (*it)->m_FieldsPtr;
			
			for ( Fields_set::iterator itMsgFields = msgFieldsPtr->begin() ; itMsgFields != msgFieldsPtr->end() ; itMsgFields++ )
			{	
				MessageComponent messageComponent;

				string	fieldName = (*itMsgFields)->Name;
				int		fieldPosition = (*itMsgFields)->Position;
				int		fieldLength = 0;
				double	dFieldValue = 0;
				string	fieldType = "";
				string	fieldValue = "";
				

				ITCHDictionary::Fields_set::iterator itFields = ITCHDictionaryPtr->m_Fields.find(fieldName);
				if ( itFields != ITCHDictionaryPtr->m_Fields.end() )
				{
					fieldLength = itFields->Length;
					fieldType = itFields->Type;
				}
							
				messageComponent.m_Name = fieldName;			
				messageComponent.m_Type = TYPE_MESSAGE;

				if ( fieldType == "X" )
				{	
					for ( int i = 0 ; i < fieldLength ; i++ )
					{
						char ch = buffer[fieldPosition + i];
						fieldValue.append(1,toascii(ch));
					}
				} 
				else if ( fieldType == "N" )
				{
					for ( int i = 0 ; i < fieldLength; i++ )
					{					
						dFieldValue += (DWORD)(buffer[fieldPosition + i] << (8*(fieldLength - 1 - i) ) );
					}

					char szConvert[20];
					memset(szConvert,0x00,sizeof(szConvert));
					sprintf(szConvert,"%.0f",dFieldValue);				
					fieldValue = szConvert;
				}

				FIELD field(fieldName, fieldValue , fieldType, fieldPosition,fieldLength);			
				messageComponent.m_Value = field;				
				m_dictMessageComponent.insert(messageComponent);				
			}

		}
		
	}

};