#include "StdAfx.h"
#include "SocketControl.h"
#include "SoupBinTCP.h"

#define	PARENT ((SoupBinTCP *)m_ParentPtr)

namespace UMDFITCH 
{
	using boost::mutex;


	SocketControl::SocketControl(void)
	{
		m_connectionSocket = INVALID_SOCKET;		
	}

	SocketControl::SocketControl(void * pParent, Config lConfig)
	{
		m_connectionSocket = INVALID_SOCKET;		
		m_loopInterval = 0;
		m_Config = lConfig;
		m_ParentPtr = pParent;

		InitializeStateMachine();
		ConfigureStateMachine();

		_evtRun = CreateEvent(NULL,false,false,NULL);
		_socketThreadPtr.reset (
        new boost::thread (
            boost::bind (&SocketControl::MainThreadLoop, this)));
	}

	SocketControl::~SocketControl(void)
	{
		::SetEvent ( _evtRun );
	}


	void SocketControl::InitializeStateMachine()
    {	
		for (int i = 0; i < (int)SIZEOF_STATES; i++)
        {
            for (int j = 0; j < (int) SIZEOF_EVENTS; j++)
            {
				_stateMachineSocket[i][j] = st_STATE_MACHINE(SOCKETCONTROL::REMAIN, &SocketControl::nop );
            }
        }
    }

	void SocketControl::MainThreadLoop()
	{
		int actionEvent = SOCKETCONTROL::NO_EVENT;

		while ( WaitForSingleObject( _evtRun, m_loopInterval ) != WAIT_OBJECT_0)
		{
			m_state = _currentState;

			if (m_event != SOCKETCONTROL::NO_EVENT)
			{
				if (_stateMachineSocket[_currentState][m_event].i_Proximo_Estado != SOCKETCONTROL::REMAIN)
				{
					_currentState = _stateMachineSocket[_currentState][ m_event].i_Proximo_Estado;
				}
				actionEvent = m_event;

				m_event = SOCKETCONTROL::NO_EVENT;

				(this->*_stateMachineSocket[m_state][actionEvent].acao)();
			}

			MonitorEvents();
		}		
		
	}

	void SocketControl::MonitorEvents()
	{
		int bytesToReceive = 0;
		m_loopInterval = SOCKETCONTROL::THREAD_LOOP_INTERVAL;

		if ( m_IncomingMessageQueue.size() > 0 )
		{
			string strEvent = GetInEvent();
			if ( !strEvent.compare("EVT_START"))
			{
				m_event = ONSTART;
			} 
			else if ( !strEvent.compare("EVT_STOP"))
			{
				m_event = ONSTOP;
			}
		}

		if ( m_state == CONNECTED )
		{
			if ( WaitToReceive() )
			{
				m_loopInterval = 1;
				SetOutEvent("EVT_SOCKET_DATA");
			}
		}
	}

	//
	//  Verifica se tem alguma coisa para receber
	//
	int SocketControl::WaitToReceive()
	{
		FD_SET		fdsReceive;
		FD_SET		fdsExcept;
		TIMEVAL		tvReceive = {0, 0};
		int			nSockets;
		
		//mError = 0;
		
		FD_ZERO(&fdsReceive);
		FD_ZERO(&fdsExcept);
		FD_SET(m_connectionSocket, &fdsReceive);
		FD_SET(m_connectionSocket, &fdsExcept);
		
		//  Verifica se tem alguma coisa para receber
		nSockets = select(NULL, &fdsReceive, NULL, &fdsExcept, &tvReceive);
		
		//  Saiu por timeout
		if (nSockets == 0) 
		{
			//mError = WSAETIMEDOUT;			
			return 0;
		}

		//  Verifica se ocorreu algum erro
		if( (nSockets == SOCKET_ERROR) || FD_ISSET(m_connectionSocket, &fdsExcept) )
		{
			//mError = WSAGetLastError();		
			m_event = ONERROR;
			SetOutEvent("EVT_SOCKET_ERROR");
			return 0;
		}

		return 1;
		
	}

	unsigned long	 SocketControl::GetSockAvailableBytes()
	{
		unsigned long	BytesToRead = 0;
		ioctlsocket( m_connectionSocket , FIONREAD, &BytesToRead );

		return BytesToRead;

	}

	int SocketControl::Receive(unsigned char *Buffer, int	BytesToRead)
	{
		int				BytesRecebidos;		
		int				TotalRecebido = 0;
		DWORD			dw_res;		
		int				mError = 0;		
		
		while(TotalRecebido < BytesToRead) 
		{			
			
			BytesRecebidos = recv(	m_connectionSocket , (char *)&Buffer[TotalRecebido], (BytesToRead - TotalRecebido) ,0);
			
			if ( BytesRecebidos == 0 ) 
			{				
				mError = WSAEINVAL;
				m_event = ONERROR;
				SetOutEvent("EVT_SOCKET_ERROR");
				return -1;
			}

			if ( WSAGetLastError() == WSAEINVAL ) 
			{				
				mError = WSAEINVAL;
				m_event = ONERROR;
				SetOutEvent("EVT_SOCKET_ERROR");
				return -1;
			}

			if ( BytesRecebidos == SOCKET_ERROR ) 
			{			
				dw_res = WSAGetLastError();
				
				if ( dw_res != WSAEWOULDBLOCK ) 
				{
					mError = dw_res;
					m_event = ONERROR;
					SetOutEvent("EVT_SOCKET_ERROR");
					return BytesRecebidos;
				}
			}
			
			TotalRecebido += BytesRecebidos;
		}

		return TotalRecebido;
	}

	/// Improve this function quality !!!
	void SocketControl::Send(unsigned char * p_buffer, int size)
	{
		send( m_connectionSocket, (char *)p_buffer, size,0);
	}


	/// <summary>
	///	PUSH FRONT
	/// </summary>
	void SocketControl::SetOutEvent(string strEvent)
	{
		
		boost::mutex::scoped_lock lockKeeper(PARENT->m_IncommingQueueLock);

		PARENT->m_IncomingMessageQueue.push_front(strEvent);
		
	}

	/// <summary>
	///	GET BACK
	/// </summary>
	string SocketControl::GetInEvent()
	{
		string strEvent;

		boost::mutex::scoped_lock lockKeeper(m_IncommingQueueLock);

		strEvent = m_IncomingMessageQueue.back();
		m_IncomingMessageQueue.pop_back();

		return strEvent;
	}
	

	void SocketControl::LogStateMachine()
	{
		/*
		GW_TRACE("");
        GW_TRACE("                              INFINITE CONNECTION ATTEMPTS SOCKET AUTOMATON                                         ");
        GW_TRACE("                              ---------------------------------------------                                         ");
        GW_TRACE("                                                                                                                    ");
        GW_TRACE("                        -----------------                                                                          ");
        GW_TRACE("                        |               |              ONSTOP /                                                    ");
        GW_TRACE("   -------------------->|    IDLE       |<---------------------------------------------------                      ");
        GW_TRACE("   |                    |               |            DisconnectDestroyTimer()               |                      ");
        GW_TRACE("   |                    -----------------                                                   |                      ");
        GW_TRACE("   |                            |                                                           |                      ");
        GW_TRACE("   |                   ONSTART  | / Connect()       ONERROR /                               |                      ");
        GW_TRACE("   |                            |               CreateTimer(RECONNECTION_INTERVAL)          |                      ");
        GW_TRACE("   |                            |         ___________________                               |                      ");
        GW_TRACE("   |                            v        /                  _\\|                             |                      ");
        GW_TRACE("   |                    -----------------                    ----------------------         |                      ");
        GW_TRACE("   |   ONSTOP           |               |                    |                    |         |                      ");
        GW_TRACE("   |<-------------------| TRY CONNECT   |                    |  WAIT TO RECONNECT |----------                      ");
        GW_TRACE("   |                    |               |                    |                    |                                ");
        GW_TRACE("   |                    ---------------- _   ONTIMEOUT       ----------------------                                ");
        GW_TRACE("   |                          |         |\\___________________/            ^                                        ");
        GW_TRACE("   |                          |                  /                        |                                        ");
        GW_TRACE("   |               ONCONNECT  |         ConnectDestroyTimer()     ONERROR | / CreateTimer(RECONNECTION_INTERVAL)   ");
        GW_TRACE("   |                   /      |                                           |                                        ");
        GW_TRACE("   |       CreatePingTimer()  v                                           |                                        ");
        GW_TRACE("   |                    -----------------                                 |                                        ");
        GW_TRACE("   |   ONSTOP           |               |                                 |                                        ");
        GW_TRACE("   |--------------------|   CONNECTED   |_________________________________|                                        ");
        GW_TRACE("                        |               |                ON_BUF_END                                                ");
        GW_TRACE("       Disconnect()     -----------------                             /                                            ");
        GW_TRACE("                                                DropConnectionAndCreateTimer(RECONNECTION_INTERVAL)                ");
        GW_TRACE("");
		*/
	}

	void SocketControl::ConfigureStateMachine()
    {            

        /*                    CURRENT STATE             EVENT 	                            NEXT STATE                      ACTION      */

		_stateMachineSocket[       IDLE       ][     ONSTART      ] = st_STATE_MACHINE(   TRY_CONNECT     ,         &SocketControl::Connect       );

		_stateMachineSocket[   TRY_CONNECT    ][     ONSTOP       ] = st_STATE_MACHINE(       IDLE        ,         &SocketControl::Disconnect    );
		_stateMachineSocket[   TRY_CONNECT    ][     ONCONNECT    ] = st_STATE_MACHINE(     CONNECTED     ,         &SocketControl::ConnectionOK  );
		_stateMachineSocket[   TRY_CONNECT    ][     ONERROR      ] = st_STATE_MACHINE( WAIT_TO_RECONNECT ,         &SocketControl::CreateTimer   );

		_stateMachineSocket[ WAIT_TO_RECONNECT][     ONSTOP       ] = st_STATE_MACHINE(       IDLE        ,         &SocketControl::DisconnectDestroyTimer   );
		_stateMachineSocket[ WAIT_TO_RECONNECT][     ONTIMEOUT    ] = st_STATE_MACHINE(       IDLE        ,         &SocketControl::ConnectDestroyTimer   );


		_stateMachineSocket[    CONNECTED     ][     ONSTOP       ] = st_STATE_MACHINE(       IDLE        ,         &SocketControl::Disconnect   );
		_stateMachineSocket[    CONNECTED     ][     ONERROR      ] = st_STATE_MACHINE( WAIT_TO_RECONNECT ,         &SocketControl::CreateTimer   );
		_stateMachineSocket[    CONNECTED     ][   ON_BUF_END     ] = st_STATE_MACHINE( WAIT_TO_RECONNECT ,         &SocketControl::nop          );

		

        _currentState = (int)IDLE;
		m_event = (int)SOCKETCONTROL::NO_EVENT;
    }

	void SocketControl::nop()
	{

	}

	void SocketControl::Connect()
	{
		HOSTENT * host;

		memset( (void*)&m_SockAddr, 0, sizeof( m_SockAddr ) );
		m_connectionSocket = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);	 
		DWORD dwReUse = 1;	

		if(m_connectionSocket != INVALID_SOCKET) 
		{
			m_SockAddr.sin_family = AF_INET;
			m_SockAddr.sin_port = htons(atoi(m_Config.m_strItchPort.c_str()));

			if((m_SockAddr.sin_addr.s_addr = inet_addr( m_Config.m_strItchIP.c_str() )) == INADDR_NONE)
			{
				if( host = (HOSTENT*)gethostbyname(m_Config.m_strItchIP.c_str()) )
				{
					memcpy((void*)&m_SockAddr.sin_addr, (void*)host->h_addr_list[0], host->h_length);
				}
				else
				{					
					closesocket(m_connectionSocket);
					m_connectionSocket = 0;				
					m_event = ONERROR;
				}
			}

			if(connect(m_connectionSocket, (const struct sockaddr*)&m_SockAddr, sizeof(struct sockaddr_in)) == SOCKET_ERROR)
			{
				DWORD dwError = WSAGetLastError();
				closesocket(m_connectionSocket);
				m_connectionSocket = 0;			
				m_event = ONERROR;
			} else {
				m_event = ONCONNECT;
			}
		}
	}

	void SocketControl::ConnectionOK()
	{
		SetOutEvent("EVT_CONNECTED");
	}

	void SocketControl::Disconnect()
	{

	}

	void SocketControl::CreateTimer()
	{

	}

	void SocketControl::ConnectDestroyTimer()
	{

	}

	void SocketControl::DisconnectDestroyTimer()
	{

	}
};