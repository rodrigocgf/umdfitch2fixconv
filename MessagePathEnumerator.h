#pragma once
#include "MessagePath.h"
#include "MessagePathElement.h"
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>

namespace UMDFITCH 
{
	using std::string;
	using boost::shared_ptr;

	class MessagePathEnumerator : public boost::enable_shared_from_this<MessagePathEnumerator>
	{
	public:
		//MessagePathEnumerator(void * p_MessagePath);
		MessagePathEnumerator(shared_ptr<MessagePath> messagePathPtr);
		~MessagePathEnumerator(void);

		void SetParent(shared_ptr<void> messagePathPtr);
		boost::shared_ptr<MessagePathEnumerator> f()
		{
			return shared_from_this();
		}

		string GetUpdatedPathElement();
		shared_ptr<MessagePathEnumerator> Clone();
		void Reset();
		bool MoveNext();
		bool HasMoreElements();
		string GetCleanNamespace();
		shared_ptr<MessagePathElement> Current();
	private:
		
		shared_ptr<MessagePath>		m_pMessagePath;
		string		m_pathAsNamespaceBuilder;
		int			m_index;
	};


}